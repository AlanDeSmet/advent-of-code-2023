#! /usr/bin/python
import sys

from collections import namedtuple

WIDTH = 7 # empty spaces between walls
LEFT_START = 2 # blank columns between left wall and new rock
BOTTOM_START = 3 # blank lines between highest rock/floor and new rock

SHAPES_RAW = """\
####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
"""


def parse_raw_shapes(raw):
    shapes = []
    for shape in raw.split("\n\n"):
        newshape = []
        shape = shape.strip()
        for line in shape.split("\n"):
            line = line.strip()
            newline = []
            for char in line:
                if char == "#":
                    newline.append(True)
                else:
                    assert char == "."
                    newline.append(False)
            newshape.append(newline)
        width = len(newshape[0])
        for idx, row in enumerate(newshape):
            assert width == len(row), f"first row of shape {len(shapes)+1} was {width} wide, but row {idx+1} is {len(row)}"
        newshape.reverse()
        # 0 is the bottom, infinity it the top, so we need to flip them.
        shapes.append(newshape)
    return shapes

def print_shape(shape):
    for line in shape:
        for char in line:
            if char:
                sys.stdout.write("■")
            else:
                sys.stdout.write("·")
        sys.stdout.write("\n")

SHAPES = parse_raw_shapes(SHAPES_RAW)

def next_shape():
    while True:
        for shape in SHAPES:
            yield shape

def next_jet(jets):
    while True:
        for jet in jets:
            yield jet


Pt = namedtuple("Pt", ["x","y"])


class Cave:
    # x=0 is the left wall
    # y=0 is the floor
    def __init__(self, max_rocks, jets):
        self.max_rocks = max_rocks
        tallest_shape = max([len(x) for x in SHAPES])
        tallest_possible = tallest_shape * max_rocks + BOTTOM_START
        self.cave = []
        self.cave.append([True]*(WIDTH+2)) # floor
        for i in range(1,tallest_possible):
            self.cave.append([True]+[False]*(WIDTH)+[True]) # with walls
        self.active = None
        self.pos = None
        self.shape = next_shape()
        self.jet = next_jet(jets)
        self.rocks_spawned = 0
        self.first_empty_row = 1

    def update_first_empty_row(self):
        start_at = max(0, self.first_empty_row-1)
        for idx in range(start_at,len(self.cave)):
            row = self.cave[idx]
            if max(row[1:WIDTH+1]) == False:
                self.first_empty_row = idx
                return idx
        assert False

    def spawn_rock(self):
        assert self.active is None
        assert self.pos is None
        self.active = next(self.shape)
        x = LEFT_START + 1 # +1 because left wall is 0
        shape_height = len(self.active)
        y = self.first_empty_row + BOTTOM_START
        self.pos = Pt(x,y)
        self.rocks_spawned += 1

    def is_active_here(self, x, y, pos = None):
        assert self.active is not None
        if pos is None: pos = self.pos
        x -= pos.x
        y -= pos.y
        if y >= len(self.active): return False
        if y < 0: return False
        if x >= len(self.active[y]): return False
        if x < 0: return False
        return self.active[y][x]

    def print_cave(self, stream):
        top_active_row = self.first_empty_row
        if self.pos is not None:
            top_active_row = max(top_active_row, self.pos.y+len(self.active)-1)
        for y in range(top_active_row, -1, -1):
            out = ""
            for x in range(WIDTH+2):
                if x == 0 and y != 0: out += "|"
                elif x == 0 and y == 0: out += "+"
                elif x == (WIDTH+1) and y != 0: out += "|"
                elif x == (WIDTH+1) and y == 0: out += "+"
                elif y == 0: out += "-"
                elif self.active is not None and self.is_active_here(x,y): out += "@"
                elif self.cave[y][x]: out += "#"
                else: out += "."
            stream.write(out+"\n")


    def collides(self, pos):
        if pos.x <= 0 or pos.x+len(self.active[0]) >= WIDTH+2:
            return True
        if pos.y >= self.first_empty_row:
            return False
        for y, row in enumerate(self.active):
            for x, cell in enumerate(row):
                cave_x = x + pos.x
                cave_y = y + pos.y
                if self.cave[cave_y][cave_x] and cell:
                    return True
        return False

    def land_rock(self):
        for y in range(len(self.active)):
            for x in range(len(self.active[0])):
                cave_x = x + self.pos.x
                cave_y = y + self.pos.y
                if self.is_active_here(cave_x, cave_y, self.pos):
                    assert self.cave[cave_y][cave_x] == False
                    self.cave[cave_y][cave_x] = True
        self.pos = None
        self.active = None
        self.update_first_empty_row()

    def step(self, stream_detailed, stream_new_rocks):
        if self.active is None:
            self.spawn_rock()
            if stream_detailed is not None:
                stream_detailed.write("A new rock begins falling:\n")
                self.print_cave(stream_detailed)
                stream_detailed.write("\n")
            if stream_new_rocks is not None:
                self.print_cave(stream_new_rocks)
                stream_new_rocks.write("\n")

        jet = next(self.jet)
        msg = "Jet of gas pushes rock "
        if jet == "<":
            newpos = Pt(self.pos.x-1,self.pos.y)
            msg += "left"
        else:
            assert jet == ">"
            newpos = Pt(self.pos.x+1,self.pos.y)
            msg += "right"
        if self.collides(newpos):
            msg += ", but nothing happens"
        else:
            self.pos = newpos
        if stream_detailed is not None:
            stream_detailed.write(msg+":\n")
            self.print_cave(stream_detailed)
            stream_detailed.write("\n")

        msg = "Rock falls 1 unit"
        newpos = Pt(self.pos.x,self.pos.y-1)
        if self.collides(newpos):
            msg += ", causing it to come to rest"
            self.land_rock()
        else:
            self.pos = newpos
        if stream_detailed is not None:
            stream_detailed.write(msg+":\n")
            self.print_cave(stream_detailed)
            stream_detailed.write("\n")
            


            




if __name__ == '__main__':
    for shape in SHAPES:
        print_shape(shape)
        sys.stdout.write("\n")


