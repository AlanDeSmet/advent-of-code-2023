#! /usr/bin/python3
import re
import collections

PtTuple = collections.namedtuple("Pt", ["x","y"])

class Pt (PtTuple):
    def __add__(self, other): return Pt(self.x+other.x, self.y+other.y)

UP   = Pt( 0,-1)
RIGHT= Pt(+1, 0)
DOWN = Pt( 0,+1)
LEFT = Pt(-1, 0)

class Cell:
    def __init__(self, up=False, right=False, down=False, left=False):
        self.up    = up
        self.right = right
        self.down  = down
        self.left  = left

    def offsets(self):
        ret = []
        if self.up:    ret.append(UP)
        if self.right: ret.append(RIGHT)
        if self.down:  ret.append(DOWN)
        if self.left:  ret.append(LEFT)
        return ret

    def simplechar(self):
        count = self.up+self.right+self.down+self.left
        if count == 0: return "."
        if count > 1: return str(count)
        if self.up: return "^"
        if self.right: return ">"
        if self.down: return "v"
        if self.left: return "<"
        assert False

    def nicechar(self):
        idx = self.up*1 + self.right*2 + self.down*4 + self.left*8
              #           111111
              # 0123456789012345
              #  ^ ^ ^ ^ ^ ^ ^ ^   
              #   >>  >>  >>  >>   
              #     vvvv    vvvv
              #         <<<<<<<<   
        return "·╵╶└╷│┌├╴┘─┴┐┤┬┼"[idx]


def load(stream):
    first = next(stream)
    assert re.match(r"#\.#+$", first)
    width = len(first)

    lines = stream.read().rstrip().split("\n")
    expect = "#"*(width-2)+".#"
    assert lines[-1] == "#"*(width-3)+".#"
    lines = lines[:-1]

    grid = []
    for line in lines:
        row = []
        line = line[1:-1] # slice walls
        for cell in line:
            if   cell == "^": row.append(Cell(up   = True))
            elif cell == ">": row.append(Cell(right= True))
            elif cell == "v": row.append(Cell(down = True))
            elif cell == "<": row.append(Cell(left = True))
            else:             row.append(Cell())
        grid.append(row)

    return grid



def step(grid):
    newgrid = []
    height = len(grid)
    width = len(grid[0])
    for y in range(height):
        newrow = []
        for x in range(len(grid[y])):
            y_up    = (y-1)%height
            x_right = (x+1)%width
            y_down  = (y+1)%height
            x_left  = (x-1)%width
            up    = grid[y_down][x      ].up
            right = grid[y     ][x_left ].right
            down  = grid[y_up  ][x      ].down
            left  = grid[y     ][x_right].left
            newrow.append(Cell(up=up, right=right, down=down, left=left))
        newgrid.append(newrow)
    return newgrid


def as_strings(grid):
    ret = []
    ret.append( "#."+"#"*len(grid[0]))
    for row in grid:
        out = "#"
        for cell in row:
            out += cell.simplechar()
            #out += cell.nicechar()
        out += "#"
        ret.appent(out)
    ret.append("#"*len(grid[0])+".#")
    return ret

def print_grid(grid):
    for line in as_strings(grid):
        print(line)


def main():
    import sys

    grid = load(open("test.input"))
    width = len(grid[0])
    height = len(grid)

    #sys.stdout.write("\33[2J")

    entrance = Pt(0,-1)
    exit = Pt(width,height+1)

    #sys.stdout.write("\033[H")
    print_grid(grid)

    

if __name__ == '__main__':
    import sys
    sys.exit(main())

#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#


#.########################################################################################################################
#>v<^^><<>^>^>v<>>^^.v>^vv<^v<v><vv.^^^^v<v<v><<^v.<>^^^>>>>><^<.vvvvv.^v>vv<>v<v<^>^v<^v.<>>v<><v^v>.v>^vv^>><v>v>.^>><>#
#<.v<v>>>.>>>>^<vv><^^v>>v>^><v^>^<.<v^<>>><v<vv<<^^<^^v^<.>v.v>>.v<^><v<v<>^><^>^>><.<<<<^>^^^v.v.>v><^<^v^>.>^>>vv.v^<<#
#<<>v>^<<^v^<<<vv.>^^>^^v^>>v<>v>v<><^^v<^>><^<>^^v^vv<v<<<>^v^v<.<^>^<>>>.<><<<v<v<<<vv<>>v.^^<v.vv<^<^><^<vv^v<v<v>v.^<#
#.v^v<v<>>>.v^vv<<v^<vv<<vvv^>^^v.v^v^^v<>^^^^^><.v^v>>v>>^>vvv^^<<^>^v^.v<^<<^v^v>>vv.<<^<<v<v>>^v>^>v.v^><><vvv^>v<>^<<#
#<v<^<v<^<>v>>v^>>^<<>>v.^^<>vv^<><.v^>><<^<v><<^>>^^^<vv>^<v>.>><<.v^v^<v>^<v>>>>vvv<><^><<<v>v>v.^<<.<v^<>vv<v<<>v>^>v>#
#.^.<>v>v^v<v>>v<>v>vv<.^><^v<<><>^.^>>v<^v.v^^^^<vv>^<vv^>vv^>^.<>.<^^v<>><^^^.><^v>v^^<>v>>v><>>v.^^<>>v<<^<^<v<vv.>^<<#
#>^v^>^v>...^<>^.<v>.v<v<>^^v<<<^.v>v>^.><<.v<v<>.>v>>v>>><<v<><^.v>.>>>^>^>.v<>>>v<^^>^v<<^v>^v^<v.v>><>><vvv^v>v^v<v<<<#
#<^.vv><<><>v^^<^.<^<><..v.^vv<v.vvv><v<^v>v<^v>v^v^^^^<>^^^.^^^v^v>>v^>><>v>^<>v^^.^<^^<v>>v^vvvv>>v^><^<<>>>^<<^v^^<<><#
#<>><^<^<<^v^>>>v<.>..v>.^>>^v^<<v^<<vv<^v^><<.><^^><.vvv^>>.>>^<^vv><.v>^^>>>.>^vv<^<<>vv<>vv^^^v.>>>><..v>.^<^v<>v.<vv<#
#<<.<><>.<^<<<<^>^<>^vv^>^>^>vv>>v>>v^>.^.>vv<><.v><<>>vvv.v^^vv>>>v<v<><^<^<>^.v<^^>^><.>>^^^^^vv<<^>^<<v<^v^<.v<<v<<.<<#
#>>v>.^.<><>^.vvv<vv>^>.^^.v>^v.v>^<vv^v>v>^v^vv<<>><v.<><<v><>^<^<v<>.<^<^^<>^<>>v^v.^<v^^<v^>v<<^.^>>^v>^^^>vv.v.>^v>^>#
#>^<>v^^^v<v^vv>>v.^>>>^v>><>^v^><v^.v><<>>><^<<.<>v<><><^v^>^^<^^>>^v^>^>^vv^<>>>^v^<.v^^^v<v<v...>^>^<^^v<v^^v>^^>.<^^>#
#<v<v^^v<>><<<^vvv<vv>>.^.<>>.^v^^<>>v^^>v><>v><v<^^.v^>>.vv^v><>^^v<>>^>^<<^.>v<v^vvv^.><<<v.^^<vvv^<.>>^.<<vv^<<<>.^>><#
#>.>^.v.>>^<v><><^<.>^v<><>v^^<.>vvv<><>.<v<>.><<.^v^^v^.>vvvv^>.<>><<^v<<>^<<<><<>>^<^>^v<^v<>v^>v<v<.>^<^vv<>^<^>>^>^<<#
#<.^.>^vv<><>^^<^<.<>vv.>^<<<>v><.<<v<>^<>>v<^^<v^v^^>v<v<^<vv<.v^<>v<^>>.<>^<^.vv><.><v<^<<^<<>^<>v<vvv^v^^>><v>v>..v<<>#
#<^v>><<v.^^.<^>^<^vvv.^v^v<>vv^v<^vvvvvv^v><>>.^<<^v>v.v.>.^<<^^>>>v^><v^>>>.v^^>.>><^.^^.>.>v^<v^>^<<v<<^><>v<^^>^<v<v<#
#.v><v>>>v^>^^v<>^<<>>^<v>^><<v^><^.<<v<vv<<^v^^><>><^^><><^^^vv>.^>.<v.^v<v<^<<.v.>>>><<<^v<<.<^<>v.^<^<vvv<.v<.v><^>v<>#
#<v<v^<^v><<^<>^>>>>^^v<>^^>v<v.^^^^^.^<^<<^><<v^><^.><^><v^>.^>v^v>v.><^<^>v<>^>.<>^..<<>^^>^<><^<^..vv>v^^^v>^^<..^.v<>#
#>v<<v<<<^.^vvv>>v.>v^v>v<><v.^><^v<>^v<v^v<v<.^^.>v>.v>vvvv^>vv^>^^v><v^>>>.<v^>><.<v><<.>>^^v^vvv<vv^^^<^>v^>vv>^>^vv>>#
#><<v>^>>>>vv<<><>^>^v>^vv^<^v><>v^.<.^.v>^<<^v>v<.^v.>^v<^^^>v^^.^>vv.<v^>>^.><<>>^<<>.>v.v>>^..>><<..v^>><>v<>.<^v.>>^<#
#.<<^^<.^v>>>>.>^vv<^<<>>>>^v>v<^.v^<^>>v^<^>^^>>v<>^<v^^^^>><^<^^>>v<.<<v.^v<>>.vv^^^<v^^.^v.<>^>^>><><^.vv<v>v>v<^^>.<>#
#<<<v>.^v>v>v<^^>vv^<vv<vv><v>v<vv<.>vv<^^^<.v<>..^<<<<<^^^>><v^^>>^<<^v^>><.^<^.<^.<v<vv<..^.><<>vv<^<v<v.<>.<<^v<<^>vv<#
#.>^^.<<>.>>>.<^<>.<<v<^>v>v>v^v^<v>v^<^>^<<>v^^v^v^^^<>^vvv^vv<.^^>.v^<.>>^>vvv^>.v^>v^v^>.^^<v<<vv^^>v>^vvvvv<vvv^<<>>>#
#<^v^.<<<><^<<v^^^v^^^<^^<<>.^<^v<.>.^^..^v<<^>^.<<>>.<^>v.^<v>.v^^.<vv^^>v^><v^><^.>>>>>^v<v<^vv^^>^v^.^.^^^>v^<.><>^^>>#
#.v^vv.^v>vv>>v<vv^^v<v>><.<>><^^>^v.vvvv>><^^>v^v>.v^<.<^.<^<^v.><v>^v<<^^<>vv>>>^v^v^<^<^^^>..<><^<^.vv<<<v>v^.^>^<>^><#
########################################################################################################################.#


