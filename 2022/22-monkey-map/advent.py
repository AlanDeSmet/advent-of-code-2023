#! /usr/bin/python3

from pathlib import Path
import unittest

class ArbitraryNameUsedInMessages(unittest.TestCase):

    def setUp(self):
        pass # Called before every test_ function

    def tearDown(self):
        pass # Called after every test_ function (provided setUp succeeded)

    def slurp(self, filename):
        # Can't use open(filename).read(), as it generates a ResourceWarning
        # under unittest
        with open(filename) as f:
            return f.read()

    def test_basic(self):
        import unittest.mock
        from tempfile import TemporaryDirectory
        import sys
        with TemporaryDirectory(prefix="advent.py-test") as dirname:
            dirname = Path(dirname)
            with open(dirname/"input-1357", "w") as f:
                f.write("test input")
            args = ["TESTING", str(dirname/"input-1357"), str(dirname/"outdir-7531")]
            with unittest.mock.patch.object(sys, 'argv', args):
                h = initialize(2022, 16, 1, "Test Case", ["file1","file2"])
            h.result.write("is_result")
            h.result.close() # avoid ResourceWarning under unittest
            h.file1.write("is_file_1")
            h.file1.close() # avoid ResourceWarning under unittest
            h.file2.write("is_file_2")
            h.file2.close() # avoid ResourceWarning under unittest
            self.assertEqual(self.slurp(dirname/"outdir-7531/result"), "is_result")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file1"), "is_file_1")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file2"), "is_file_2")
            self.assertEqual(h.input.read(), "test input")
            h.input.close() # avoid ResourceWarning under unittest


    def test_equality(self):
        """ Some simple equality tests

        The first line in this docstring will be used in output"""
        self.assertEqual(1,1)
        self.assertNotEqual(1,2, "One and Two are the same")

        self.assertAlmostEqual(3.14, 3.13, places=1)
        self.assertNotAlmostEqual(3.14, 3.13, places=2)

        self.assertGreater(3,2)
        self.assertGreaterEqual(2,2)
        self.assertLess(2,3)
        self.assertLessEqual(2,2)


#def load_tests(loader, tests, ignore):
#    """ Ensure unittest runs doctest """
#    import doctest
#    tests.addTests(doctest.DocTestSuite())
#    return tests


def parse_args(desc):
    """ Parse arguments, return results of argparse.ArgumentParser.parse_args() """
    import argparse

    p = argparse.ArgumentParser(description=desc)

    p.add_argument('input', metavar='FILE',
                   type=argparse.FileType('r'),
                   help='input file from Advent of Code')
    p.add_argument('outdir', metavar='DIR',
                   type=Path,
                   help='directory to place output in')

    args = p.parse_args()

    if args.outdir.exists() and not args.outdir.is_dir():
        p.error(f"{args.outdir} exists but is not a directory")

    try:
        args.outdir.mkdir(exist_ok=True)
    except Exception as e:
        p.error(f"Unable to create directory {args.outdir} because {e}")

    return args

def initialize(year, day, part, title, outfiles = []):
    import re
    assert "result" not in outfiles
    outfiles += ["result"]
    desc = f"Solver for Advent of Code {year} day {day} part {part} ({title})"
    args = parse_args(desc)

    assert args.input not in outfiles

    class Files:
        pass
    handles = Files()
    handles.input = args.input
    for path in outfiles:
        fuller_path = args.outdir/path
        attrname = re.sub(r'[^A-Za-z0-9]+', '_', path)
        assert len(attrname)
        assert attrname[0] not in "0123456789"
        handle =open(fuller_path,"w")
        setattr(handles, attrname, handle)

    return handles


if __name__ == '__main__':
    unittest.main()
