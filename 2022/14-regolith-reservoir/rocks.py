#! /usr/bin/python3
import sys

EMPTY="."
WALL="#"
START="+"
SAND="o"
FLOOR="="

def str_to_pt(s):
    parts = s.split(",")
    return list([int(x) for x in parts])

def get_playfield_active_height(playfield):
    for rownum in range(len(playfield)-1, -1, -1):
        row = playfield[rownum]
        if row != [EMPTY]*len(row):
            return rownum


def get_playfield_active_x_range(playfield):
    min_x = 500
    max_x = 500
    for row in playfield:
        this_min_x = 500
        this_max_x = 500
        for x in range(0,501):
            if row[x] != EMPTY and row[x] != FLOOR:
                min_x = min(min_x, x)
        for x in range(len(row)-1, 499, -1):
            if row[x] != EMPTY and row[x] != FLOOR:
                max_x = max(max_x, x)
    return min_x, max_x

def show_playfield(playfield, active=(0,-1), stream=sys.stdout):
    height = get_playfield_active_height(playfield)
    min_x, max_x = get_playfield_active_x_range(playfield)
    #print(f"range {min_x},0 through {max_x, height}")
    for rownum in range(height+1):
        out = ""
        for colnum in range(min_x, max_x+1):
            if rownum == active[1] and colnum == active[0]:
                out += "x"
            elif rownum == 0 and colnum == 500:
                out += START
            else:
                out += playfield[rownum][colnum]
        stream.write(out+"\n")


#460,0 - 521,166

def load_rocks(stream):
    playfield = []
    for i in range(200):
        playfield.append([EMPTY]*1000)

    for line in stream:
        line = line.rstrip()
        points = line.split(" -> ")

        cur_x, cur_y = str_to_pt(points.pop(0))
        playfield[cur_y][cur_x] = WALL

        for point in points:
            dst_x,dst_y = str_to_pt(point)
            while cur_x != dst_x or cur_y != dst_y:
                if cur_x < dst_x: cur_x += 1
                if cur_x > dst_x: cur_x -= 1
                if cur_y < dst_y: cur_y += 1
                if cur_y > dst_y: cur_y -= 1
                playfield[cur_y][cur_x] = WALL
    return playfield

def add_floor(playfield):
    floor_row = get_playfield_active_height(playfield) + 2
    playfield[floor_row] = [FLOOR]*len(playfield[floor_row])

def drop_sand(playfield):
    max_y = get_playfield_active_height(playfield)
    x,y = 500,0
    while y <= max_y:
        #print(x,y)
        #show_playfield(playfield, (x,y))
        if playfield[y+1][x] == EMPTY:
            y+=1
        elif playfield[y+1][x-1] == EMPTY:
            y+=1
            x-=1
        elif playfield[y+1][x+1] == EMPTY:
            y+=1
            x+=1
        else:
            playfield[y][x] = SAND
            return True
    return False



def main():
    playfield = load_rocks(sys.stdin)
    show_playfield(playfield)


if __name__ == '__main__':
    sys.exit(main())
