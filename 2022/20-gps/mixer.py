#! /usr/bin/python3

class Entry:
    def __init__(self, idx, val):
        self.idx = idx
        self.val = val
    def __repr__(self):
        return str(self.val)

def fmtnum(numbers):
    return ", ".join([str(x.val) for x in numbers])

def find_orig_idx(target, numbers):
    for idx, entry in enumerate(numbers):
        if entry.idx == target:
            return idx
    assert False

def find_number(target, numbers):
    for idx, entry in enumerate(numbers):
        if entry.val == target:
            return idx
    assert False

def load(input, multiplier = 1):
    numbers = []
    for idx, line in enumerate(input):
        numbers.append(Entry(idx, int(line)*multiplier))
    return numbers

def mix(numbers, output):
    for orig_idx in range(0, len(numbers)):
        idx = find_orig_idx(orig_idx, numbers)

        if numbers[idx].val == 0:
            if output:
                output.write("0 does not move:\n")
        else:
            entry = numbers.pop(idx)
            newidx_tmp = idx + entry.val
            newidx = newidx_tmp % len(numbers)
            if newidx == 0: newidx = len(numbers) 
            #print(f"I want to move index {idx} value {entry.val} to {newidx_tmp} which cycles to {newidx}")
            numbers.insert(newidx, entry)
            before_idx = (newidx-1)%len(numbers)
            before_val = numbers[before_idx].val
            after_idx = (newidx+1)%len(numbers)
            after_val = numbers[after_idx].val

            if output:
                output.write(f"{entry.val} moves between {before_val} and {after_val}:\n")
        if output:
            output.write(fmtnum(numbers)+"\n\n")

