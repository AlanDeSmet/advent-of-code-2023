#! /usr/bin/python3

from dataclasses import dataclass
import re
import operator

MATHSYM = {
        operator.add: "+",
        operator.mul: "×",
        }
MATHDESC = {
        operator.add: "increases by",
        operator.mul: "is multiplied by",
        }

PRIMES =  [2, 3, 5, 7, 11, 13, 17, 19, 23]

def simplify_item(item):
    assert len(item) <= len(PRIMES)
    for idx in range(len(item)):
        val = item[idx]
        prime = PRIMES[idx]
        item[idx] = val % prime
    return item


@dataclass
class Monkey:
    monkey_id: int
    items: str
    operation: str
    secondarg: int
    divisible_by: int
    true_target: int
    false_target: int
    inspections: int = 0

    def __str__(self):
        op = MATHSYM[self.operation]
        second = self.secondarg
        if second is None:
            second = "old"

        return f"Monkey(#{self.monkey_id}: holding: {','.join(str(x) for x in self.items)}, operation old{op}{second}, test % {self.divisible_by}, if true:->{self.true_target}, if false:->{self.false_target}):"

    def initial_items(self, items):
        self.items = []
        for item in items:
            newitem = simplify_item([item]*len(PRIMES))
        self.items.append(newitem)

    def inspect(self, monkeys, dbg):
        dbg.write(f"Monkey {self.monkey_id}:\n")
        while(len(self.items)):
            item = self.items.pop(0)
            assert len(item) == len(PRIMES)
            self.inspections += 1
            dbg.write(f"  Monkey inspects an item with a worry level of {item}.\n")
            if self.secondarg is not None:
                secondary = [self.secondarg]*len(PRIMES)
                desc_second = secondary
            else:
                secondary = item
                desc_second = "itself"
            newitem = []
            #print("item", item)
            #print("seco", secondary, "(",desc_second,")")
            for lhs,rhs in zip(item, secondary):
                #print(lhs,rhs)
                newitem.append(self.operation(lhs,rhs))
            #print("new ", newitem)
            dbg.write(f"    Worry level {MATHDESC[self.operation]} {desc_second} to {newitem}.\n")
            newitem = simplify_item(newitem)
            myslot = PRIMES.index(self.divisible_by)
            if newitem[myslot] != 0:
                is_div = "is not"
                target = self.false_target
            else:
                is_div = "is"
                target = self.true_target
            dbg.write(f"    Current worry level {is_div} divisible by {self.divisible_by}.\n")
            dbg.write(f"    Item with worry level {newitem} is thrown to monkey {target}.\n")
            monkeys[target].items.append(newitem)

def parse_monkey(lines):
    m = re.match(r'Monkey (\d+):', lines[0])
    assert m
    monkey_id = int(m.group(1))
    m = re.match(r'  Starting items: ([\d, ]+)', lines[1])
    assert m
    items = [int(x) for x in m.group(1).split(", ")]
    m = re.match(r'  Operation: new = old (.) (\S+)', lines[2])
    assert m
    operator_str = m.group(1)
    if operator_str == "+":
        operatorfn = operator.add
    else:
        assert operator_str == "*"
        operatorfn = operator.mul
    secondarg_str = m.group(2)
    if secondarg_str == "old":
        secondarg = None
    else:
        secondarg = int(secondarg_str)
    m = re.match(r'  Test: divisible by (\d+)', lines[3])
    assert m
    divisible_by = int(m.group(1))
    m = re.match(r'    If true: throw to monkey (\d+)', lines[4])
    assert m
    true_target = int(m.group(1))
    m = re.match(r'    If false: throw to monkey (\d+)', lines[5])
    assert m
    false_target = int(m.group(1))
    ret = Monkey(monkey_id, [], operatorfn, secondarg, divisible_by, true_target, false_target)
    ret.initial_items(items)
    return ret

def parse_monkeys(lines):
    monkeys = []
    for i in range(0,len(lines),7):
        monkey = parse_monkey(lines[i:i+6])
        monkeys.append(monkey)
    return monkeys

def parse_input(filename):
    lines = open(filename).readlines()
    return parse_monkeys(lines)

def describe_monkeys(monkeys, round, dbg):
    if (round+1) in [15,20]:
        dbg.write("...\n\n")
    dbg.write(f"After round {round+1}, the monkeys are holding items with these worry levels:\n")
    for monkey in monkeys:
        dbg.write(f"Monkey {monkey.monkey_id}: "+", ".join([str(x) for x in monkey.items])+"\n")
    dbg.write("\n")
