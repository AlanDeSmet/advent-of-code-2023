#! /usr/bin/python3

from pathlib import Path
import unittest
import sys
from dataclasses import dataclass
from collections import Counter

class ArbitraryNameUsedInMessages(unittest.TestCase):

    def setUp(self):
        pass # Called before every test_ function

    def tearDown(self):
        pass # Called after every test_ function (provided setUp succeeded)

    def slurp(self, filename):
        # Can't use open(filename).read(), as it generates a ResourceWarning
        # under unittest
        with open(filename) as f:
            return f.read()

    def test_basic(self):
        import unittest.mock
        from tempfile import TemporaryDirectory
        import sys
        with TemporaryDirectory(prefix="advent.py-test") as dirname:
            dirname = Path(dirname)
            with open(dirname/"input-1357", "w") as f:
                f.write("test input")
            args = ["TESTING", str(dirname/"input-1357"), str(dirname/"outdir-7531")]
            with unittest.mock.patch.object(sys, 'argv', args):
                h = initialize(2022, 16, 1, "Test Case", ["file1","file2"])
            h.result.write("is_result")
            h.result.close() # avoid ResourceWarning under unittest
            h.file1.write("is_file_1")
            h.file1.close() # avoid ResourceWarning under unittest
            h.file2.write("is_file_2")
            h.file2.close() # avoid ResourceWarning under unittest
            self.assertEqual(self.slurp(dirname/"outdir-7531/result"), "is_result")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file1"), "is_file_1")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file2"), "is_file_2")
            self.assertEqual(h.input.read(), "test input")
            h.input.close() # avoid ResourceWarning under unittest


    def test_equality(self):
        """ Some simple equality tests

        The first line in this docstring will be used in output"""
        self.assertEqual(1,1)
        self.assertNotEqual(1,2, "One and Two are the same")

        self.assertAlmostEqual(3.14, 3.13, places=1)
        self.assertNotAlmostEqual(3.14, 3.13, places=2)

        self.assertGreater(3,2)
        self.assertGreaterEqual(2,2)
        self.assertLess(2,3)
        self.assertLessEqual(2,2)


#def load_tests(loader, tests, ignore):
#    """ Ensure unittest runs doctest """
#    import doctest
#    tests.addTests(doctest.DocTestSuite())
#    return tests


def parse_args(desc, moreargs):
    """ Parse arguments, return results of argparse.ArgumentParser.parse_args() """
    import argparse

    p = argparse.ArgumentParser(description=desc)

    p.add_argument('input', metavar='FILE',
                   type=argparse.FileType('r'),
                   help='input file from Advent of Code')
    p.add_argument('outdir', metavar='DIR',
                   type=Path,
                   help='directory to place output in')
    if moreargs is not None: moreargs(p)

    args = p.parse_args()

    if args.outdir.exists() and not args.outdir.is_dir():
        p.error(f"{args.outdir} exists but is not a directory")

    try:
        args.outdir.mkdir(exist_ok=True)
    except Exception as e:
        p.error(f"Unable to create directory {args.outdir} because {e}")

    return args

class SmartInput:
    def __init__(self, file):
        self.file = file

    def read(self, size=-1): return self.file.read(size)
    def readline(self, size=-1): return self.file.readline(size)
    def readlines(self, hint=-1): return self.file.readlines(hint)
    def close(self): self.file.close()

    def sreadline(self):
        return self.file.readline().rstrip("\n")

    def sreadlines(self):
        return self.file.read().rstrip().split("\n")

class SmartOutput:
    def __init__(self, file):
        self.file = file

    def print(self, *args, **kwargs):
        kwargs["file"] = self
        print(*args, **kwargs)

    def write(self, text): self.file.write(text)
    def close(self): self.file.close()

def initialize(year, day, part, title, outfiles = [], moreargs = None):
    import re
    assert "result" not in outfiles
    outfiles += ["result"]
    part_desc = f"part {part}"
    if part == 3:
        part_desc = "parts 1 and 2"
    desc = f"Solver for Advent of Code {year} day {day} {part_desc} ({title})"
    args = parse_args(desc, moreargs)

    assert args.input not in outfiles

    class Files:
        pass
    handles = Files()
    handles.input = SmartInput(args.input)
    for path in outfiles:
        fuller_path = args.outdir/path
        attrname = re.sub(r'[^A-Za-z0-9]+', '_', path)
        assert len(attrname)
        assert attrname[0] not in "0123456789"
        handle = open(fuller_path,"w")
        smarthandle = SmartOutput(handle)
        setattr(handles, attrname, smarthandle)

    if moreargs is not None:
        return handles, args

    return handles

################################################################################
@dataclass(frozen=True)
class Pt:
    x: int = 0
    y: int = 0

    def manhattan_distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

    def __add__(self, other): return Pt(self.x+other.x, self.y+other.y)
    def __sub__(self, other): return Pt(self.x-other.x, self.y-other.y)
    def __mul__(self, other): return Pt(self.x*other, self.y*other)
    def __neg__(self): return Pt(-self.x, -self.y)
    def __eq__(self, other): return self.y == other.y and self.x == other.x
    def __lt__(self, other):
        if self.y != other.y:
            return self.y < other.y
        return self.x < other.x
    def __repr__(self): return f"({self.x},{self.y})"

class TestPt(unittest.TestCase):
    def test_construction(self):
        p = Pt()
        self.assertEqual((p.x, p.y), (0,0))
        p = Pt(15,9283022)
        self.assertEqual((p.x, p.y), (15,9283022))

    def test_failed_member_assignment(self):
        p = Pt(15,9283022)
        with self.assertRaises(AttributeError):
            p.x = 99
        with self.assertRaises(AttributeError):
            p.y = 123

    def test_add(self):
        p1 = Pt(123, 456)
        p2 = Pt(1000,2000)
        p = p1 + p2
        self.assertEqual(p, Pt(1123, 2456))

    def test_sub(self):
        p1 = Pt(10, 20)
        p2 = Pt(1000,2000)
        p = p2 - p1
        self.assertEqual(p, Pt(990, 1980))

    def test_neg(self):
        self.assertEqual(-Pt(3,7), Pt(-3, -7))

    def test_eq(self):
        self.assertEqual(Pt(123,456), Pt(123,456))
        self.assertEqual(Pt(123,456), Pt(23,56)+Pt(100,400))
        self.assertNotEqual(Pt(123,456), Pt(122,456))
        self.assertNotEqual(Pt(123,456), Pt(123,457))

    def test_lt(self):
        self.assertTrue( Pt(10,0) < Pt(11,0))
        self.assertFalse(Pt(10,0) < Pt(10,0))
        self.assertFalse(Pt(10,0) < Pt(9,0))

        self.assertTrue( Pt(5,10) < Pt(6,10))
        self.assertFalse(Pt(5,10) < Pt(5,10))
        self.assertFalse(Pt(5,10) < Pt(4,10))
        self.assertFalse(Pt(5,10) < Pt(99,9))

    def test_repr(self):
        self.assertEqual(repr(Pt(123,456)), "(123,456)")
        self.assertEqual(repr(Pt(-3,7)), "(-3,7)")
        self.assertEqual(repr(Pt(3,-7)), "(3,-7)")
        self.assertEqual(repr(-Pt(3,7)), "(-3,-7)")

    def test_hashable(self):
        set().add(Pt(3,5))

################################################################################
@dataclass(frozen=True)
class Pt3:
    x: int = 0
    y: int = 0
    z: int = 0

    def manhattan_distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y) + abs(self.z - other.z)

    def __add__(self, other): return Pt3(self.x+other.x, self.y+other.y, self.z+other.z)
    def __sub__(self, other): return Pt3(self.x-other.x, self.y-other.y, self.z-other.z)
    def __mul__(self, other): return Pt3(self.x*other, self.y*other, self.z*other)
    def __neg__(self): return Pt3(-self.x, -self.y, -self.z)
    def __eq__(self, other): return self.y == other.y and self.x == other.x and self.z == other.z
    def __lt__(self, other):
        if self.z != other.z:
            return self.z < other.z
        if self.y != other.y:
            return self.y < other.y
        return self.x < other.x
    def __repr__(self): return f"({self.x},{self.y},{self.z})"

class TestPt3(unittest.TestCase):
    def test_construction(self):
        p = Pt3()
        self.assertEqual((p.x, p.y, p.z), (0,0,0))
        p = Pt3(15,9283022,99)
        self.assertEqual((p.x, p.y, p.z), (15,9283022,99))

    def test_failed_member_assignment(self):
        p = Pt3(15,9283022)
        with self.assertRaises(AttributeError):
            p.x = 99
        with self.assertRaises(AttributeError):
            p.y = 123
        with self.assertRaises(AttributeError):
            p.z = 123

    def test_add(self):
        p1 = Pt3(123, 456, 1)
        p2 = Pt3(1000,2000, 2)
        p = p1 + p2
        self.assertEqual(p, Pt3(1123, 2456, 3))

    def test_sub(self):
        p1 = Pt3(100, 4000, 1)
        p2 = Pt3(1000,2000, 2)
        p = p2 - p1
        self.assertEqual(p, Pt3(900, -2000, 1))

    def test_neg(self):
        self.assertEqual(-Pt3(3,7,-2), Pt3(-3, -7, 2))

    def test_eq(self):
        self.assertEqual(Pt3(123,456,789), Pt3(123,456,789))
        self.assertEqual(Pt3(123,456,10), Pt3(23,56,5)+Pt3(100,400,5))
        self.assertNotEqual(Pt3(123,456,789), Pt3(122,456,789))
        self.assertNotEqual(Pt3(123,456,789), Pt3(123,457,789))
        self.assertNotEqual(Pt3(123,456,789), Pt3(123,456,788))

    def test_lt(self):
        self.assertTrue( Pt3(10,0,0) < Pt3(11,0,0))
        self.assertFalse(Pt3(10,0,0) < Pt3(10,0,0))
        self.assertFalse(Pt3(10,0,0) < Pt3(9,0,0))

        self.assertTrue( Pt3(5,10,1) < Pt3(6,10,1))
        self.assertFalse(Pt3(5,10,1) < Pt3(5,10,1))
        self.assertFalse(Pt3(5,10,1) < Pt3(4,10,1))
        self.assertFalse(Pt3(5,10,1) < Pt3(99,9,1))

        self.assertTrue( Pt3(5,10,1) < Pt3(5,11,1))
        self.assertFalse(Pt3(5,10,1) < Pt3(5, 9,1))
        self.assertTrue( Pt3(5,10,1) < Pt3(5,10,2))
        self.assertFalse(Pt3(5,10,1) < Pt3(5,10,0))
        self.assertFalse(Pt3(5,10,1) < Pt3(5,20,0))

    def test_repr(self):
        self.assertEqual(repr(Pt3(123,456,789)), "(123,456,789)")
        self.assertEqual(repr(Pt3(-3,7,9)), "(-3,7,9)")
        self.assertEqual(repr(Pt3(3,-7,9)), "(3,-7,9)")
        self.assertEqual(repr(-Pt3(3,7,9)), "(-3,-7,-9)")
        self.assertEqual(repr(Pt3(3,7,-9)), "(3,7,-9)")

    def test_hashable(self):
        set().add(Pt3(3,5,3))

################################################################################
class Range:
    def __init__(self, start, end):
        assert start <= end, f"Range's start must be before range's end, but {start} > {end}"
        self.start = start
        self.end = end


    def overlaps(self, other):
        # 0123456789
        #  --==
        if self.end <= other.start: return False
        if self.start >= other.end: return False
        return True

    def adjacent(self, other):
        return self.end == other.start or self.start == other.end

    def overlaps_or_adjacent(self, other):
        return self.overlaps(other) or self.adjacent(other)

    def __repr__(self): return f"[{self.start}-{self.end})"
    def __len__(self): return self.end - self.start

    def __getitem__(self, idx):
        if idx >= len(self) or idx < 0:
            raise IndexError(f"Range object index ({idx}) out of range {self.start-self.end}")
        return idx + self.start

    def __contains__(self, val):
        return val >= self.start and val < self.end

    def __or__(self, other):
        assert self.overlaps_or_adjacent(other), f"Attempting to merge two ranges that are not overlapping or adjacent: {self} {other}"
        return Range(min(self.start, other.start), max(self.end, other.end))

    def __and__(self, other):
        if not self.overlaps(other):
            return Range(0,0)
        return Range(max(self.start, other.start), min(self.end, other.end))

    def __eq__(self, other):
        return self.start == other.start and self.end == other.end

    def copy(self):
        return Range(self.start, self.end)

    def cut_out(self, mask):
        """ Returns list of parts of self excluding mask

        If the mask entirely encompasses self, will return an empty list.
        If the mask encompasses one end, will return a single item list.
        Otherwise returns a list of two things.
        """
        if not self.overlaps(mask): return [self.copy()]
        overlap = self & mask
        result = []
        before = Range(self.start, overlap.start)
        if len(before) > 0: result.append(before)
        after = Range(overlap.end, self.end)
        if len(after) > 0: result.append(after)
        return result

class TestRange(unittest.TestCase):
    def test_construction(self):
        r = Range(-3,-1)
        self.assertEqual((r.start, r.end), (-3,-1))
        r = Range(-3,-3)
        self.assertEqual((r.start, r.end), (-3,-3))
        with self.assertRaises(AssertionError):
            Range(-1, -3)

    def test_member_assignment(self):
        r = Range(-3,-1)
        self.assertEqual((r.start, r.end), (-3,-1))
        r.start = -2
        self.assertEqual((r.start, r.end), (-2,-1))
        r.end = 22
        self.assertEqual((r.start, r.end), (-2,22))

    def test_overlaps(self):
        def tst_overlap(overlaps, start1, end1, start2, end2):
            a = Range(start1, end1)
            b = Range(start2, end2)
            if overlaps:
                self.assertTrue(a.overlaps(b))
                self.assertTrue(b.overlaps(a))
            else:
                self.assertFalse(a.overlaps(b))
                self.assertFalse(b.overlaps(a))
        tst_overlap(True, 1,3, 2.999,3)
        tst_overlap(True, 1,10, 1,10)
        tst_overlap(True, 1,10, 2,3)
        tst_overlap(False, 1,3, 3,5)
        tst_overlap(False, 1,3, 4,5)

    def test_adjacent(self):
        def tst_adjacent(adjacent, start1, end1, start2, end2):
            a = Range(start1, end1)
            b = Range(start2, end2)
            if adjacent:
                self.assertTrue(a.adjacent(b))
                self.assertTrue(b.adjacent(a))
            else:
                self.assertFalse(a.adjacent(b))
                self.assertFalse(b.adjacent(a))
        tst_adjacent(True, 1,3, 3,9)
        tst_adjacent(False, 1,2.999, 3,9)
        tst_adjacent(False, 1,10, 2,9)
        tst_adjacent(False, 1,10, 2,10)
        tst_adjacent(False, 1,10, 1,9)
        tst_adjacent(False, 1,10, 1,10)

    def test_overlaps_or_adjacent(self):
        def tst(o_or_a, start1, end1, start2, end2):
            a = Range(start1, end1)
            b = Range(start2, end2)
            if o_or_a:
                self.assertTrue(a.overlaps_or_adjacent(b))
                self.assertTrue(b.overlaps_or_adjacent(a))
            else:
                self.assertFalse(a.overlaps_or_adjacent(b))
                self.assertFalse(b.overlaps_or_adjacent(a))
        tst(True, 1,3, 2.999,3)
        tst(True, 1,10, 1,10)
        tst(True, 1,10, 2,3)
        tst(True, 1,3, 3,5)
        tst(False, 1,3, 4,5)
        tst(True, 1,3, 3,9)
        tst(False, 1,2.999, 3,9)
        tst(True, 1,10, 2,9)
        tst(True, 1,10, 2,10)
        tst(True, 1,10, 1,9)
        tst(True, 1,10, 1,10)


    def test_repr(self):
        self.assertEqual(repr(Range(-2,-1)), "[-2--1)")
        self.assertEqual(repr(Range(-2,10)), "[-2-10)")

    def test_len(self):
        self.assertEqual(len(Range(0,10)), 10)
        self.assertEqual(len(Range(1,3)), 2)
        self.assertEqual(len(Range(-3,3)), 6)
        self.assertEqual(len(Range(-3,-3)), 0)

    def test_getitem(self):
        r = Range(-3, 10)
        self.assertEqual(r[0], -3)
        self.assertEqual(r[3], 0)
        self.assertEqual(r[12], 9)
        with self.assertRaises(IndexError):
            r[-1]
        with self.assertRaises(IndexError):
            r[13]

    def test_contains(self):
        r = Range(-3, 10)
        self.assertTrue(-3 in r)
        self.assertTrue(0 in r)
        self.assertTrue(9 in r)
        self.assertFalse(-4 in r)
        self.assertFalse(10 in r)

    def test_or(self):
        def tst_or(range1_start, range1_end, range2_start, range2_end, result_start, result_end):
            r1 = Range(range1_start, range1_end)
            r2 = Range(range2_start, range2_end)
            result = Range(result_start, result_end)
            self.assertEqual(r1|r2, result)
            self.assertEqual(r2|r1, result)
        tst_or(1,4, 4,8, 1,8)
        tst_or(1,4, 2,8, 1,8)
        with self.assertRaises(AssertionError):
            Range(1,4)|Range(4.00001, 5)

    def test_and(self):
        def tst_and(r1_start, r1_end, r2_start, r2_end, result_start, result_end):
            r1 = Range(r1_start, r1_end)
            r2 = Range(r2_start, r2_end)
            result = Range(result_start, result_end)
            self.assertEqual(r1&r2, result)
            self.assertEqual(r2&r1, result)

        tst_and(1,4, 4,8, 0,0)
        tst_and(1,4, 3,8, 3,4)
        tst_and(1,10, 2,4, 2,4)

    def test_eq(self):
        self.assertTrue(Range(1,4) == Range(1,4))
        self.assertFalse(Range(1,4) == Range(1,5))
        self.assertFalse(Range(1,4) == Range(2,4))
        self.assertTrue( Range(3,4) == (Range(1,4)&Range(3,8)))

    def test_copy(self):
        r1 = Range(2,4)
        r2 = r1
        r3 = r1.copy()
        r1.start = 1
        self.assertEqual(Range(1,4), r1)
        self.assertEqual(Range(1,4), r2)
        self.assertEqual(Range(2,4), r3)

    def test_cut_out(self):
        r1 = Range(3,6)

        # Cut out everything
        res = r1.cut_out(Range(3,6))
        self.assertEqual(len(res), 0)

        res = r1.cut_out(Range(1,10))
        self.assertEqual(len(res), 0)

        # Cut out nothing
        res = r1.cut_out(Range(0,3))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(3,6))

        res = r1.cut_out(Range(6,10))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(3,6))

        # Cut out one end
        res = r1.cut_out(Range(3,5))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(5,6))

        res = r1.cut_out(Range(1,5))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(5,6))

        res = r1.cut_out(Range(5,6))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(3,5))

        res = r1.cut_out(Range(5,10))
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], Range(3,5))

        # Cut out middle
        r1 = Range(10,100)
        res = r1.cut_out(Range(55,65))
        self.assertEqual(len(res), 2)
        self.assertTrue(Range(10,55) in res)
        self.assertTrue(Range(65,100) in res)




################################################################################
class Grid:
    """ Implements a rectangular grid of data """

    def __init__(self, rows):
        """ Create Grid, pass in 2d list-like data

        rows should be an iterable with each item returned representing a row
        of data. Those rows should themselves be iterable, with each item
        returned being an element of data.  All rows must be the same length.
        """
        if len(rows) == 0: raise ValueError("Grid cannot hold an empty grid")
        if rows[0][-1] == "\n": raise ValueError("First line ends with a newline; you should strip those before passing to Grid")
        width = len(rows[0])
        self._rows = []
        for y, row in enumerate(rows):
            if len(row) != width: raise ValueError(f"Grid's input must be rectangular, but row 0 is {width} long but row {y} is {len(row)} long")
            self._rows.append(list(row))

    def height(self):
        """ How many rows are in the data? """
        return len(self._rows)
    def width(self):
        """ How many columns are in the data? """
        return len(self._rows[0])

    def row(self, idx):
        """ Return list of data on row idx """
        if idx < 0 or idx >= self.height():
            raise IndexError(f"row {idx} is out of range 0-{self.height()-1}")
        return self._rows[idx]

    def rows(self):
        """ Return list of rows, each entry of which is a list of data in that row """
        return self._rows

    def column(self, idx):
        """ Return list of data on column idx """
        if idx < 0 or idx >= self.width():
            raise IndexError(f"column {idx} is out of range 0-{self.width()-1}")
        return list([row[idx] for row in self._rows])

    def columns(self):
        """ Return list of columns, each entry of which is a list of data in that column """
        return [self.column(idx) for idx in range(self.width())]

    def row_as_str(self, idx):
        """ Return data in row idx as a string, elements all jammed together """
        return "".join([str(x) for x in self.row(idx)])

    def rows_as_strlist(self):
        """ Return list of all rows of data, each entry being a string of the data in that row, elements all jammed together """
        return [self.row_as_str(idx) for idx in range(self.height())]

    def column_as_str(self, idx):
        """ Return data in column idx as a string, elements all jammed together """
        return "".join([str(x) for x in self.column(idx)])

    def columns_as_strlist(self):
        """ Return list of all columns of data, each entry being a string of the data in that column, elements all jammed together """
        return [self.column_as_str(idx) for idx in range(self.width())]

    def is_in_bounds(self, pos):
        if isinstance(pos, tuple):
            row = pos[1]
            col = pos[0]
        elif isinstance(pos, Pt):
            row = pos.y
            col = pos.x
        else:
            raise ValueError(f"Grid.is_in_bounds, takes a #,# tuple or a Pt, not a {type(pos)}")

        return row >= 0 and row < self.height() and col >= 0 and col < self.width()

    def _get_row_col(self, pos):
        if isinstance(pos, tuple):
            row = pos[1]
            col = pos[0]
        elif isinstance(pos, Pt):
            row = pos.y
            col = pos.x
        else:
            raise ValueError(f"Grid[x], takes a #,# tuple or a Pt, not a {type(pos)}")

        if row < 0 or row >= self.height():
            raise IndexError(f"Row/y value {row} is out of range 0-{self.height()-1}")
        if col < 0 or col >= self.width():
            raise IndexError(f"Column/x value {col} is out of range 0-{self.width()-1}")

        return row, col

    def __getitem__(self, pos):
        row, col = self._get_row_col(pos)
        return self._rows[row][col]

    def __setitem__(self, pos, value):
        row, col = self._get_row_col(pos)
        self._rows[row][col] = value

    def findall(self, value):
        """ Return list of Pt()s where value was found in this grid """
        result = []
        for y, row in enumerate(self.rows()):
            for x, cell in enumerate(row):
                if cell == value:
                    result.append(Pt(x,y))
        return result

    def copy(self):
        """ Return a deep copy of myself """
        rows = []
        for row in self._rows:
            rows.append(row[:])
        return Grid(rows)

    def rotate_cw(self):
        """ Rotate data clockwise 90 degrees"""
        rows = []
        for column in self.columns():
            rows.append(list(reversed(column)))
        self._rows = rows

    def rotate_ccw(self):
        """ Rotate data counterclockwise 90 degrees"""
        self.rotate_cw()
        self.rotate_cw()
        self.rotate_cw()

    def rotate_180(self):
        """ Rotate data 180 degrees """
        self.rotate_cw()
        self.rotate_cw()

    def flip_left_right(self):
        """ Flip data left to right around a vertical axis """
        rows = []
        for row in self.rows():
            rows.append(list(reversed(row)))
        self._rows = rows

    def flip_up_down(self):
        """ Flip data up to down around a horizontal axis """
        self._rows = list(reversed(self._rows))

    def as_str_list(self, seperator = ""):
        rows = []
        for row in self._rows:
            rows.append(seperator.join([str(x) for x in row]))
        return rows

    def dump_as_str_list(self, file=sys.stdout, seperator = ""):
        for row in self.as_str_list():
            file.write(row+"\n")


################################################################################

prime_cache = [2]
max_prime_cache = 2
def primes_up_to(target):
    if target < 2:
        raise ValueError("primes_up_to requires target of at least 2")

    global prime_cache
    global max_prime_cache

    if max_prime_cache >= target:
        return [x for x in prime_cache if x <= target]

    for num in range(max_prime_cache+1,target+1):
        #print(f"consider {num}")
        max_prime_cache = num
        is_prime = True
        for div in prime_cache:
            #print(f"  vs {div}")
            if (num % div) == 0:
                is_prime = False
                break
        if is_prime:
            prime_cache.append(num)
    return prime_cache

def factor(target):
    if target < 2:
        raise ValueError("factor requires target of at least 2")
    factors = []
    remainder = target
    test_against = 2
    while remainder > 1:
        if (remainder%test_against) == 0:
            factors.append(test_against)
            remainder /= test_against
        else:
            test_against += 1
    return factors

def least_common_multiple(*numbers):
    if len(numbers) == 1:
        numbers = numbers[0]
    if isinstance(numbers, int):
        raise ValueError(f"least_common_multiple needs at least 2 values, received 1")
    if len(numbers) < 2:
        raise ValueError(f"least_common_multiple needs at least 2 values, received {len(numbers)}")

    first = numbers[0]
    second = numbers[1]
    gcd = greatest_common_divisor(first, second)
    last = abs(first*second)//gcd

    for number in numbers[2:]:
        gcd = greatest_common_divisor(last, number)
        last = abs(last*number)//gcd
    return last


def greatest_common_divisor(*numbers):
    if len(numbers) == 1:
        numbers = numbers[0]
    common = None
    for number in numbers:
        this_one = Counter([1])
        if number > 1:
            this_one = Counter(factor(number))
        if common is None:
            common = this_one
        else:
            common &= this_one

    result = 1
    for this_factor in common.elements():
        result *= this_factor
    return result



class TestMathTools(unittest.TestCase):
    def test_primes_up_to(self):
        self.assertEqual(primes_up_to(18), [2,3,5,7,11,13,17])
        self.assertEqual(primes_up_to(2), [2])
        self.assertEqual(primes_up_to(4), [2,3])
        with self.assertRaises(ValueError):
            primes_up_to(1)

    def test_factor(self):
        self.assertEqual(factor(2), [2])
        self.assertEqual(factor(382), [2, 191])
        self.assertEqual(factor(191), [191])
        self.assertEqual(factor(2*2*3*3*7*13), [2,2,3,3,7,13])
        self.assertEqual(factor(2*2*3*3*7*13*7919), [2,2,3,3,7,13,7919])
        with self.assertRaises(ValueError):
            factor(0)
        with self.assertRaises(ValueError):
            factor(1)

    def test_greatest_common_divisor(self):
        gcd = greatest_common_divisor
        self.assertEqual(gcd(2,4), 2)
        self.assertEqual(gcd([2,4]), 2)
        self.assertEqual(gcd(2*2*3*3*7*13,2*2*7*13*5), 2*2*7*13)
        self.assertEqual(gcd(3,5*7), 1)
        self.assertEqual(gcd(2*3*3*5*13,2*3*5*17,2*5*17), 2*5)

    def test_least_common_multiple(self):
        lcm = least_common_multiple
        self.assertEqual(lcm(60424,193052,231614), 337721412394184)
        self.assertEqual(lcm([60424,193052,231614]), 337721412394184)
        with self.assertRaises(ValueError):
            lcm(1)
        with self.assertRaises(ValueError):
            lcm([1])







class TestGrid(unittest.TestCase):
    def test_construction(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), [list("1234"), list("5678"), list("9ABC")])
        with self.assertRaises(ValueError):
            Grid([])
        with self.assertRaises(ValueError):
            Grid(["1234\n", "5678\n", "9ABC\n"])
        with self.assertRaises(ValueError):
            Grid(["1234", "567", "9ABC"])

    def test_height_width(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.width(), 4)
        self.assertEqual(g.height(), 3)

    def test_rows(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), [list("1234"), list("5678"), list("9ABC")])

    def test_columns(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.columns(), [list("159"), list("26A"), list("37B"), list("48C")])

    def test_row(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(1), list("5678"))
        with self.assertRaises(IndexError):
            g.row(-1)
        with self.assertRaises(IndexError):
            g.row(3)

    def test_column(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.column(1), list("26A"))
        with self.assertRaises(IndexError):
            g.column(-1)
        with self.assertRaises(IndexError):
            g.column(4)

    def test_row_as_str(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row_as_str(1), "5678")

    def test_rows_as_strlist(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows_as_strlist(), ["1234", "5678", "9ABC"])

    def test_column_as_str(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.column_as_str(1), "26A")

    def test_columns_as_strlist(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.columns_as_strlist(), ["159", "26A", "37B", "48C"])

    def test_getitem(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g[1,2], "A")
        self.assertEqual(g[Pt(1,2)], "A")

        with self.assertRaises(ValueError): g[9]

        with self.assertRaises(IndexError): g[-1,0]
        with self.assertRaises(IndexError): g[Pt(-1,0)]
        with self.assertRaises(IndexError): g[0,-1]
        with self.assertRaises(IndexError): g[Pt(0,-1)]

        with self.assertRaises(IndexError): g[0,3]
        with self.assertRaises(IndexError): g[Pt(0,3)]
        with self.assertRaises(IndexError): g[4,0]
        with self.assertRaises(IndexError): g[Pt(4,0)]

    def test_setitem(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(0), list("1234"))
        self.assertEqual(g.row(1), list("5678"))
        self.assertEqual(g.row(2), list("9ABC"))
        g[Pt(0,0)] = "X"
        g[1,1] = "Y"
        g[3,2] = "Z"
        self.assertEqual(g.row(0), list("X234"))
        self.assertEqual(g.row(1), list("5Y78"))
        self.assertEqual(g.row(2), list("9ABZ"))

        with self.assertRaises(ValueError): g[9] = "a"

        with self.assertRaises(IndexError): g[-1,0] = "a"
        with self.assertRaises(IndexError): g[Pt(-1,0)] = "a"
        with self.assertRaises(IndexError): g[0,-1] = "a"
        with self.assertRaises(IndexError): g[Pt(0,-1)] = "a"

        with self.assertRaises(IndexError): g[0,3] = "a"
        with self.assertRaises(IndexError): g[Pt(0,3)] = "a"
        with self.assertRaises(IndexError): g[4,0] = "a"
        with self.assertRaises(IndexError): g[Pt(4,0)] = "a"

    def test_findall(self):
        g = Grid(["..#.",
                     "#..#",
                     "....",
                     ".#..",])
        hits = set(g.findall("#"))
        expected = {
                Pt(2,0),
                Pt(0,1),
                Pt(3,1),
                Pt(1,3),
                }
        self.assertEqual(hits, expected)

        hits = g.findall("X")
        self.assertEqual(len(hits), 0)

    def test_copy(self):
        g1 = Grid(["123", "456"])
        self.assertEqual(g1.row(0), list("123"))
        self.assertEqual(g1.row(1), list("456"))
        g2 = g1.copy()
        self.assertEqual(g2.row(0), list("123"))
        self.assertEqual(g2.row(1), list("456"))
        g1[0,0] = "X"
        g2[1,1] = "Y"
        self.assertEqual(g1.row(0), list("X23"))
        self.assertEqual(g1.row(1), list("456"))
        self.assertEqual(g2.row(0), list("123"))
        self.assertEqual(g2.row(1), list("4Y6"))
        
    def test_rotate_cw(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(0), list("1234"))
        self.assertEqual(g.row(1), list("5678"))
        self.assertEqual(g.row(2), list("9ABC"))
        g.rotate_cw()
        self.assertEqual(g.row(0), list("951"))
        self.assertEqual(g.row(1), list("A62"))
        self.assertEqual(g.row(2), list("B73"))
        self.assertEqual(g.row(3), list("C84"))

    def test_rotate_ccw(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.rotate_ccw()
        self.assertEqual(g.row(0), list("48C"))
        self.assertEqual(g.row(1), list("37B"))
        self.assertEqual(g.row(2), list("26A"))
        self.assertEqual(g.row(3), list("159"))

    def test_rotate_180(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.rotate_180()
        self.assertEqual(g.row(0), list("CBA9"))
        self.assertEqual(g.row(1), list("8765"))
        self.assertEqual(g.row(2), list("4321"))

    def test_flip_left_right(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.flip_left_right()
        self.assertEqual(g.row(0), list("4321"))
        self.assertEqual(g.row(1), list("8765"))
        self.assertEqual(g.row(2), list("CBA9"))

    def test_flip_up_down(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.flip_up_down()
        self.assertEqual(g.row(0), list("9ABC"))
        self.assertEqual(g.row(1), list("5678"))
        self.assertEqual(g.row(2), list("1234"))

    def test_as_str_list(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.as_str_list(), ["1234", "5678", "9ABC"])

    def test_dump_as_str_list(self):
        g = Grid(["1234", "5678", "9ABC"])
        import io
        out = io.StringIO()
        g.dump_as_str_list(out)
        self.assertEqual(out.getvalue(), "1234\n5678\n9ABC\n")



################################################################################
if __name__ == '__main__':
    unittest.main()
