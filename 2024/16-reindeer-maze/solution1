#! /usr/bin/python3

import advent
from advent import Pt

DIRS = [
        Pt(+1, 0),
        Pt(-1, 0),
        Pt( 0,+1),
        Pt( 0,-1),
        ]

def load_map(input):
    lines = input.sreadlines()
    maze = advent.Grid(lines)
    end = Pt(maze.width()-2, 1)
    start = Pt(1, maze.height()-2)
    assert maze[end] == "E", f"End position {end} doesn't hold 'E'"
    assert maze[start] == "S", f"Start position {end} doesn't hold 'S'"
    maze[end] = "."
    maze[start] = "."
    return maze, start, end

def potential_turns(dir):
    return [
            Pt(+dir.y,+dir.x),
            Pt(-dir.y,-dir.x),
            ]

files = advent.initialize(2023,16,1,"Reindeer Maze",[])

maze, start, end = load_map(files.input)

FORWARD_COST = 1
TURN_90_COST = 1000

to_consider = [ (start, Pt(+1, 0), 0) ]
nodes = {}
while len(to_consider) > 0:
    consider = to_consider.pop(0)
    pos,dir,cost = consider

    if maze[pos] == "#":
        continue

    if cost >= nodes.get((pos,dir), float('inf')):
        continue

    nodes[(pos,dir)] = cost

    to_consider.append( (pos+dir, dir, cost+FORWARD_COST) )

    turns = potential_turns(dir)
    assert len(turns) == 2
    to_consider.append( (pos, turns[0], cost+TURN_90_COST) )
    to_consider.append( (pos, turns[1], cost+TURN_90_COST) )

best_score = float('inf')
for dir in DIRS:
    if (end,dir) in nodes:
        best_score = min(nodes[(end,dir)], best_score)
files.result.write(f"{best_score}\n")
#print(best_score)
