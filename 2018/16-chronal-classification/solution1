#! /usr/bin/python3

import advent
from dataclasses import dataclass
import re

def op_addr(args, regs):
    regs[args[2]] = regs[args[0]] + regs[args[1]]
def op_addi(args, regs):
    regs[args[2]] = regs[args[0]] + args[1]
def op_mulr(args, regs):
    regs[args[2]] = regs[args[0]] * regs[args[1]]
def op_muli(args, regs):
    regs[args[2]] = regs[args[0]] * args[1]
def op_banr(args, regs):
    regs[args[2]] = regs[args[0]] & regs[args[1]]
def op_bani(args, regs):
    regs[args[2]] = regs[args[0]] & args[1]
def op_borr(args, regs):
    regs[args[2]] = regs[args[0]] | regs[args[1]]
def op_bori(args, regs):
    regs[args[2]] = regs[args[0]] | args[1]
def op_setr(args, regs):
    regs[args[2]] = regs[args[0]]
def op_seti(args, regs):
    regs[args[2]] = args[0]
def op_gtir(args, regs):
    regs[args[2]] = args[0] > regs[args[1]]
def op_gtri(args, regs):
    regs[args[2]] = regs[args[0]] > args[1]
def op_gtrr(args, regs):
    regs[args[2]] = regs[args[0]] > regs[args[1]]
def op_eqir(args, regs):
    regs[args[2]] = int(args[0] == regs[args[1]])
def op_eqri(args, regs):
    regs[args[2]] = int(regs[args[0]] == args[1])
def op_eqrr(args, regs):
    regs[args[2]] = int(regs[args[0]] == regs[args[1]])

ALL_INSTRUCTIONS = [
    op_addr,
    op_addi,
    op_mulr,
    op_muli,
    op_banr,
    op_bani,
    op_borr,
    op_bori,
    op_setr,
    op_seti,
    op_gtir,
    op_gtri,
    op_gtrr,
    op_eqir,
    op_eqri,
    op_eqrr,
        ]

@dataclass
class Example:
    reg_before: list
    reg_after: list
    instruction: list

def extract_ints(line):
    results = re.findall(r'\d+', line)
    return list([int(x) for x in results])

def burst_example(example):
    example = example.split("\n")
    assert example[0].startswith("Before:"), f"No 'Before:': {example[0]}"
    assert example[2].startswith("After:"), f"No 'After:': {example[1]}"
    reg_before = extract_ints(example[0])
    instruction = extract_ints(example[1])
    reg_after = extract_ints(example[2])
    return Example(reg_before, reg_after, instruction)

def read_file(input):
    body = input.read()
    examples,code = body.split("\n\n\n")

    examples = examples.split("\n\n")
    out_examples = []
    for example in examples:
        out_examples.append(burst_example(example))

    code = code.split("\n")
    out_code = []
    for code_line in code:
        out_code.append(extract_ints(code_line))
    return out_examples, out_code

def find_possible_instructions(example):
    possible = []
    for instruction in ALL_INSTRUCTIONS:
        registers = list(example.reg_before)
        instruction(example.instruction[1:], registers)
        if registers == example.reg_after:
            possible.append(instruction)
    return possible



files = advent.initialize(2023,16,1,"Chronal Classification",["output"])

examples, code = read_file(files.input)

at_least_three = 0
for example in examples:
    inst = find_possible_instructions(example)
    if len(inst) >= 3:
        at_least_three += 1

print(at_least_three, len(examples))
files.result.write(f"{at_least_three}\n")
