#! /usr/bin/python3

import advent
from advent import Pt
from enum import Enum

class NextTurn(Enum):
    LEFT = 1
    STRAIGHT = 2
    RIGHT = 3

class Direction(Enum):
    LEFT  = Pt(-1, 0)
    UP    = Pt( 0,-1)
    RIGHT = Pt(+1, 0)
    DOWN  = Pt( 0,+1)

class Cart:
    def __init__(self, pos, dir):
        self.pos = pos
        self.dir = dir
        self.next_turn = NextTurn.LEFT

    def __repr__(self):
        return f"<Cart @{self.pos}, going {self.dir}, turning {self.next_turn}>"

    def symbol(self):
        m = {
                Direction.LEFT: "<",
                Direction.RIGHT: ">",
                Direction.UP: "^",
                Direction.DOWN: "v",
                }
        return m[self.dir]
        

def load_data(file):
    lines = file.readlines()
    for rownum in range(len(lines)):
        if lines[rownum][-1] == "\n":
            lines[rownum] = lines[rownum][:-1]
    grid = advent.Grid(lines)
    carts = []
    for y in range(grid.height()):
        for x in range(grid.width()):
            p = Pt(x,y)
            if grid[p] == "<":
                carts.append(Cart(Pt(x,y), Direction.LEFT))
                grid[p] = "-"
            elif grid[p] == "^":
                carts.append(Cart(Pt(x,y), Direction.UP))
                grid[p] = "|"
            elif grid[p] == ">":
                carts.append(Cart(Pt(x,y), Direction.RIGHT))
                grid[p] = "-"
            elif grid[p] == "v":
                carts.append(Cart(Pt(x,y), Direction.DOWN))
                grid[p] = "|"
            else:
                assert grid[p] in "+-|/\\ ", f"Unexpected character {grid[p]}"
    return grid, carts

def remove_carts_at_position(position, carts, cidx):
    new_carts = []
    outidx = cidx
    for idx, cart in enumerate(carts):
        if cart.pos != position:
            new_carts.append(cart)
        else:
            if idx <= cidx:
                outidx -= 1
    return new_carts, outidx


def update_carts(grid, carts):
    carts.sort(key=lambda c: (c.pos.y,c.pos.x))
    cidx = 0
    while cidx < len(carts):
        cart = carts[cidx]
        cart.pos = cart.pos + cart.dir.value
        ground = grid[cart.pos]
        if ground == "-":
            assert cart.dir in [Direction.LEFT, Direction.RIGHT]
        elif ground == "|":
            assert cart.dir in [Direction.UP, Direction.DOWN]
        elif ground == "/":
            bounce = {
                    Direction.UP: Direction.RIGHT,
                    Direction.RIGHT: Direction.UP,
                    Direction.LEFT: Direction.DOWN,
                    Direction.DOWN: Direction.LEFT,
                    }
            cart.dir = bounce[cart.dir]
        elif ground == "\\":
            bounce = {
                    Direction.UP: Direction.LEFT,
                    Direction.LEFT: Direction.UP,
                    Direction.RIGHT: Direction.DOWN,
                    Direction.DOWN: Direction.RIGHT,
                    }
            cart.dir = bounce[cart.dir]
        elif ground == "+":
            turn_left = {
                    Direction.UP: Direction.LEFT,
                    Direction.LEFT: Direction.DOWN,
                    Direction.DOWN: Direction.RIGHT,
                    Direction.RIGHT: Direction.UP,
                    }
            turn_right = {
                    Direction.UP: Direction.RIGHT,
                    Direction.RIGHT: Direction.DOWN,
                    Direction.DOWN: Direction.LEFT,
                    Direction.LEFT: Direction.UP,
                    }
            if cart.next_turn == NextTurn.LEFT:
                cart.dir = turn_left[cart.dir]
                cart.next_turn = NextTurn.STRAIGHT
            elif cart.next_turn == NextTurn.STRAIGHT:
                cart.next_turn = NextTurn.RIGHT
            elif cart.next_turn == NextTurn.RIGHT:
                cart.dir = turn_right[cart.dir]
                cart.next_turn = NextTurn.LEFT
        else:
            assert False, f"Cart at {cart.pos} is on unknown terrain '{ground}'"

        crash_position = crash_pos(carts)
        if crash_position is not None:
            carts, cidx = remove_carts_at_position(crash_position, carts, cidx)
        cidx += 1

    return carts


def write_map(file, grid, carts):
    cart_dict = {}
    for cart in carts:
        if cart.pos in cart_dict:
            cart_dict[cart.pos] = "X"
        else:
            cart_dict[cart.pos] = cart.symbol()
    for y, row in enumerate(grid.rows()):
        out = ""
        for x, cell in enumerate(row):
            pt = Pt(x,y)
            if pt in cart_dict:
                out += cart_dict[pt]
            else:
                out += cell
        file.write(out+"\n")

def crash_pos(carts):
    cart_set = set()
    for cart in carts:
        if cart.pos in cart_set:
            return cart.pos
        cart_set.add(cart.pos)
    return None


files = advent.initialize(2023,13,1,"Mine Cart Madness",["output"])

grid, carts = load_data(files.input)
if len(carts) < 3: write_map(files.output, grid, carts)

counter = 0
while len(carts) > 1:
    carts = update_carts(grid, carts)
    if len(carts) < 10:
        files.output.write("\n")
        write_map(files.output, grid, carts)
    counter += 1
    print(counter, len(carts))
files.result.write(f"{carts[0].pos}\n")
