#! /usr/bin/python3

import advent
from advent import Pt
import re

class Star:
    def __init__(self, pos, vel):
        self.pos = pos
        self.vel = vel

    def update(self):
        self.pos += self.vel

def burst_line(line):
    m = re.match(r'position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>', line)
    assert m, f"Failed to match {line}"
    return (
            Pt(int(m.group(1)), int(m.group(2))),
            Pt(int(m.group(3)), int(m.group(4)))
            )

def find_star_bounds(stars):
    min_x = stars[0].pos.x
    min_y = stars[0].pos.y
    max_x = min_x
    max_y = min_y
    for star in stars:
        min_x = min(min_x, star.pos.x)
        min_y = min(min_y, star.pos.y)
        max_x = max(max_x, star.pos.x)
        max_y = max(max_y, star.pos.y)
    return Pt(min_x, min_y), Pt(max_x, max_y)

def stars_as_string_grid(stars, min_bound, max_bound):
    grid = []
    for y in range (min_bound.y, max_bound.y+1):
        grid.append(["."]*(max_bound.x-min_bound.x+1))

    for star in stars:
        grid_pos = star.pos - min_bound
        grid[grid_pos.y][grid_pos.x] = "#"

    return grid

def print_stars(file, stars, min_bound, max_bound):
    grid = stars_as_string_grid(stars, min_bound, max_bound)
    for row in grid:
        file.write("".join(row)+"\n")




def load_stars(input):
    result = []
    for line in input.sreadlines():
        pos, vel = burst_line(line)
        star = Star(pos, vel)
        result.append(star)
    return result


files = advent.initialize(2023,10,1,"The Stars Align",["output"])
stars = load_stars(files.input)

is_test_data = len(stars) < 100

if False and is_test_data:
    test_min, test_max = find_star_bounds(stars)
    files.output.write("Initially:\n")
    print_stars(files.output, stars, test_min, test_max)
    for i in range(1,4+1):
        for star in stars:
            star.update()
        files.output.write("\n")
        s = ""
        if i > 1: s = "s"
        files.output.write(f"After {i} second{s}:\n")
        print_stars(files.output, stars, test_min, test_max)

last_area = None
last_grid = None
seconds = 0
while True:
    print(seconds)
    bmin, bmax = find_star_bounds(stars)
    area = (bmax.x-bmin.x+1) * (bmax.y-bmin.y+1)
    if last_area is not None and area > last_area:
        for row in last_grid:
            files.result.write("".join(row)+"\n")
        break
    
    last_area = area
    if area < 1000000:
        last_grid = stars_as_string_grid(stars, bmin, bmax)
    for star in stars:
        star.update()
    seconds += 1






