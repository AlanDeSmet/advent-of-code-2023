#! /usr/bin/python3

from advent import Pt, Grid, Range

NORTH = Pt( 0, -1)
SOUTH = Pt( 0, +1)
EAST = Pt(+1,  0)
WEST = Pt(-1,  0)

TRANSITIONS = {
        '-': {
            NORTH: [ EAST, WEST ],
            SOUTH: [ EAST, WEST ],
            EAST:  [ EAST ],
            WEST:  [ WEST ],
            },
        '|': {
            NORTH: [ NORTH ],
            SOUTH: [ SOUTH ],
            EAST:  [ NORTH, SOUTH ],
            WEST:  [ NORTH, SOUTH ],
            },
        '/': {
            NORTH: [ EAST ],
            SOUTH: [ WEST ],
            EAST:  [ NORTH ],
            WEST:  [ SOUTH ],
            },
        '\\': {
            NORTH: [ WEST ],
            SOUTH: [ EAST ],
            EAST:  [ SOUTH ],
            WEST:  [ NORTH ],
            },
        '.': {
            NORTH: [ NORTH ],
            SOUTH: [ SOUTH ],
            EAST:  [ EAST ],
            WEST:  [ WEST ],
            },
        }

class Beam:
    def __init__(self, pos, dir):
        self.pos = pos
        self.dir = dir
    def __repr__(self):
        return f"{self.pos} going {self.dir}"

def count_lit(grid, start_pos, start_dir, output = None):
    bounds_x = Range(0, grid.width())
    bounds_y = Range(0, grid.height())
    todo = [ Beam( start_pos, start_dir ) ]

    lit = set()
    done = set()

    while len(todo):
        task = todo.pop(0)

        if (task.pos, task.dir) in done:
            # Don't redo done work (avoids loops)
            continue
        done.add((task.pos, task.dir))

        lit.add(task.pos)
        char = grid[task.pos]
        out_dirs = TRANSITIONS[char][task.dir]
        for dir in out_dirs:
            new_pos = task.pos + dir
            if new_pos.x not in bounds_x: continue
            if new_pos.y not in bounds_y: continue
            new_beam = Beam(new_pos, dir)
            todo.append(new_beam)

    if output is not None:
        for y in range(grid.height()):
            for x in range(grid.width()):
                if Pt(x,y) in lit:
                    output.write("#")
                else:
                    output.write(".")
            output.write("\n")
    return len(lit)
