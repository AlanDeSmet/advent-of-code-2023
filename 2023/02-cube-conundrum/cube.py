#! /usr/bin/python3

import re

class Counts:
    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

    def __str__(self):
        return f"{self.red}, {self.green}, {self.blue}"

class Game:
    def __init__(self, number, rounds):
        self.number = number
        self.rounds = rounds
    def __str__(self):
        return f"{self.number}: {'; '.join([str(x) for x in self.rounds])}"

def split_colors(colorstr):
    colors = colorstr.split(", ")
    red = green = blue = 0
    for color in colors:
        count, name = color.split(" ")
        count = int(count)
        if name == "red":   assert red==0;   red   = count
        if name == "green": assert green==0; green = count
        if name == "blue":  assert blue==0;  blue  = count
    return Counts(red, green, blue)

def parse_line(line):
    m = re.match(r"Game (\d+): (.*)", line)
    gamenum = int(m.group(1))
    rounds = m.group(2).split("; ")
    rounds = list([split_colors(round) for round in rounds])
    return Game(gamenum, rounds)

def max_colors(rounds):
    maxes = Counts(0,0,0)
    for round in rounds:
        maxes.red = max(maxes.red, round.red)
        maxes.green = max(maxes.green, round.green)
        maxes.blue = max(maxes.blue, round.blue)
    return maxes


