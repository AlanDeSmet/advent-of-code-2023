#! /usr/bin/python3

import advent
from advent import StrGrid

files = advent.initialize(2023,13,1,"Point of Incidence",["output"])

def is_mirror_at(slices, idx):
    size = len(slices)
    slices_to_check = min(idx+1, size-idx-1)
    for offset in range(slices_to_check):
        #print(f"considering {offset}: {idx-offset} - {idx+1+offset} (max {size})")
        if slices[idx-offset] != slices[idx+1+offset]:
            return False
    return True

def find_mirror(slices):
    # -1 because we test slices[idx] against slice[idx+1]
    for idx in range(len(slices)-1):
        if is_mirror_at(slices, idx):
            return idx
    return None
        

total = 0
for pattern in files.input.read().rstrip().split("\n\n"):
    grid = StrGrid(list([x.rstrip() for x in pattern.split("\n")]))

    rows = grid.rows()
    idx = find_mirror(rows)
    if idx is not None:
        files.output.write(f"horizontal {idx+1}\n")
        total += (idx+1) * 100
    else:
        columns = grid.columns()
        idx = find_mirror(columns)
        assert idx is not None
        files.output.write(f"vertical {idx+1}\n")
        total += idx+1

files.result.write(f"{total}\n")
