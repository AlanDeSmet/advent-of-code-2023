#! /usr/bin/python3

from pathlib import Path
import unittest
import sys

class ArbitraryNameUsedInMessages(unittest.TestCase):

    def setUp(self):
        pass # Called before every test_ function

    def tearDown(self):
        pass # Called after every test_ function (provided setUp succeeded)

    def slurp(self, filename):
        # Can't use open(filename).read(), as it generates a ResourceWarning
        # under unittest
        with open(filename) as f:
            return f.read()

    def test_basic(self):
        import unittest.mock
        from tempfile import TemporaryDirectory
        import sys
        with TemporaryDirectory(prefix="advent.py-test") as dirname:
            dirname = Path(dirname)
            with open(dirname/"input-1357", "w") as f:
                f.write("test input")
            args = ["TESTING", str(dirname/"input-1357"), str(dirname/"outdir-7531")]
            with unittest.mock.patch.object(sys, 'argv', args):
                h = initialize(2022, 16, 1, "Test Case", ["file1","file2"])
            h.result.write("is_result")
            h.result.close() # avoid ResourceWarning under unittest
            h.file1.write("is_file_1")
            h.file1.close() # avoid ResourceWarning under unittest
            h.file2.write("is_file_2")
            h.file2.close() # avoid ResourceWarning under unittest
            self.assertEqual(self.slurp(dirname/"outdir-7531/result"), "is_result")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file1"), "is_file_1")
            self.assertEqual(self.slurp(dirname/"outdir-7531/file2"), "is_file_2")
            self.assertEqual(h.input.read(), "test input")
            h.input.close() # avoid ResourceWarning under unittest


    def test_equality(self):
        """ Some simple equality tests

        The first line in this docstring will be used in output"""
        self.assertEqual(1,1)
        self.assertNotEqual(1,2, "One and Two are the same")

        self.assertAlmostEqual(3.14, 3.13, places=1)
        self.assertNotAlmostEqual(3.14, 3.13, places=2)

        self.assertGreater(3,2)
        self.assertGreaterEqual(2,2)
        self.assertLess(2,3)
        self.assertLessEqual(2,2)


#def load_tests(loader, tests, ignore):
#    """ Ensure unittest runs doctest """
#    import doctest
#    tests.addTests(doctest.DocTestSuite())
#    return tests


def parse_args(desc, moreargs):
    """ Parse arguments, return results of argparse.ArgumentParser.parse_args() """
    import argparse

    p = argparse.ArgumentParser(description=desc)

    p.add_argument('input', metavar='FILE',
                   type=argparse.FileType('r'),
                   help='input file from Advent of Code')
    p.add_argument('outdir', metavar='DIR',
                   type=Path,
                   help='directory to place output in')
    if moreargs is not None: moreargs(p)

    args = p.parse_args()

    if args.outdir.exists() and not args.outdir.is_dir():
        p.error(f"{args.outdir} exists but is not a directory")

    try:
        args.outdir.mkdir(exist_ok=True)
    except Exception as e:
        p.error(f"Unable to create directory {args.outdir} because {e}")

    return args

class SmartInput:
    def __init__(self, file):
        self.file = file

    def readline(size=-1): return self.file.readline(size)
    def readlines(hint=-1): return self.file.readlines(hint)

    def sreadline(self):
        return self.file.readline().rstrip("\n")

    def sreadlines(self):
        return self.file.read().rstrip().split("\n")

class SmartOutput:
    def __init__(self, file):
        self.file = file
    def print(self, *args, **kwargs):
        kwargs["file"] = self
        print(*args, **kwargs)
    def write(self, text): self.file.write(text)

def initialize(year, day, part, title, outfiles = [], moreargs = None):
    import re
    assert "result" not in outfiles
    outfiles += ["result"]
    part_desc = f"part {part}"
    if part == 3:
        part_desc = "parts 1 and 2"
    desc = f"Solver for Advent of Code {year} day {day} {part_desc} ({title})"
    args = parse_args(desc, moreargs)

    assert args.input not in outfiles

    class Files:
        pass
    handles = Files()
    handles.input = SmartInput(args.input)
    for path in outfiles:
        fuller_path = args.outdir/path
        attrname = re.sub(r'[^A-Za-z0-9]+', '_', path)
        assert len(attrname)
        assert attrname[0] not in "0123456789"
        handle = open(fuller_path,"w")
        smarthandle = SmartOutput(handle)
        setattr(handles, attrname, smarthandle)

    if moreargs is not None:
        return handles, args

    return handles

################################################################################
class Pt:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def manhatten_distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

    def __add__(self, other): return Pt(self.x+other.x, self.y+other.y)
    def __neg__(self): return Pt(-self.x, -self.y)
    def __eq__(self, other): return self.y == other.y and self.x == other.x
    def __lt__(self, other):
        if self.y != other.y:
            return self.y < other.y
        return self.x < other.x
    def __repr__(self): return f"({self.x},{self.y})"
    def __hash__(self): return self.x + self.y*10000000

class TestPt(unittest.TestCase):
    def test_construction(self):
        p = Pt()
        self.assertEqual((p.x, p.y), (0,0))
        p = Pt(15,9283022)
        self.assertEqual((p.x, p.y), (15,9283022))

    def test_member_assignment(self):
        p = Pt(15,9283022)
        p.x = 99
        self.assertEqual((p.x, p.y), (99,9283022))
        p.y = 123
        self.assertEqual((p.x, p.y), (99,123))

    def test_add(self):
        p1 = Pt(123, 456)
        p2 = Pt(1000,2000)
        p = p1 + p2
        self.assertEqual(p, Pt(1123, 2456))

    def test_neg(self):
        self.assertEqual(-Pt(3,7), Pt(-3, -7))

    def test_eq(self):
        self.assertEqual(Pt(123,456), Pt(123,456))
        self.assertEqual(Pt(123,456), Pt(23,56)+Pt(100,400))
        self.assertNotEqual(Pt(123,456), Pt(122,456))
        self.assertNotEqual(Pt(123,456), Pt(123,457))

    def test_lt(self):
        self.assertTrue( Pt(10,0) < Pt(11,0))
        self.assertFalse(Pt(10,0) < Pt(10,0))
        self.assertFalse(Pt(10,0) < Pt(9,0))

        self.assertTrue( Pt(5,10) < Pt(6,10))
        self.assertFalse(Pt(5,10) < Pt(5,10))
        self.assertFalse(Pt(5,10) < Pt(4,10))
        self.assertFalse(Pt(5,10) < Pt(99,9))

    def test_repr(self):
        self.assertEqual(repr(Pt(123,456)), "(123,456)")
        self.assertEqual(repr(Pt(-3,7)), "(-3,7)")
        self.assertEqual(repr(Pt(3,-7)), "(3,-7)")
        self.assertEqual(repr(-Pt(3,7)), "(-3,-7)")

################################################################################
class Range:
    def __init__(self, start, end):
        assert start <= end, f"Range's start must be before range's end, but {start} > {end}"
        self.start = start
        self.end = end


    def overlaps(self, other):
        # 0123456789
        #  --==
        if self.end <= other.start: return False
        if self.start >= other.end: return False
        return True

    def adjacent(self, other):
        return self.end == other.start or self.start == other.end

    def overlaps_or_adjacent(self, other):
        return self.overlaps(other) or self.adjacent(other)

    def __repr__(self): return f"[{self.start}-{self.end})"
    def __len__(self): return self.end - self.start

    def __getitem__(self, idx):
        if idx >= len(self) or idx < 0:
            raise IndexError(f"Range object index ({idx}) out of range {self.start-self.end}")
        return idx + self.start

    def __contains__(self, val):
        return val >= self.start and val < self.end

    def __or__(self, other):
        assert self.overlaps_or_adjacent(other), f"Attempting to merge two ranges that are not overlapping or adjacent: {self} {other}"
        return Range(min(self.start, other.start), max(self.end, other.end))

    def __and__(self, other):
        if not self.overlaps(other):
            return Range(0,0)
        return Range(max(self.start, other.start), min(self.end, other.end))

    def __eq__(self, other):
        return self.start == other.start and self.end == other.end

class TestRange(unittest.TestCase):
    def test_construction(self):
        r = Range(-3,-1)
        self.assertEqual((r.start, r.end), (-3,-1))
        r = Range(-3,-3)
        self.assertEqual((r.start, r.end), (-3,-3))
        with self.assertRaises(AssertionError):
            Range(-1, -3)

    def test_member_assignment(self):
        r = Range(-3,-1)
        self.assertEqual((r.start, r.end), (-3,-1))
        r.start = -2
        self.assertEqual((r.start, r.end), (-2,-1))
        r.end = 22
        self.assertEqual((r.start, r.end), (-2,22))

    def test_overlaps(self):
        def tst_overlap(overlaps, start1, end1, start2, end2):
            a = Range(start1, end1)
            b = Range(start2, end2)
            if overlaps:
                self.assertTrue(a.overlaps(b))
                self.assertTrue(b.overlaps(a))
            else:
                self.assertFalse(a.overlaps(b))
                self.assertFalse(b.overlaps(a))
        tst_overlap(True, 1,3, 2.999,3)
        tst_overlap(True, 1,10, 1,10)
        tst_overlap(True, 1,10, 2,3)
        tst_overlap(False, 1,3, 3,5)
        tst_overlap(False, 1,3, 4,5)

    def test_repr(self):
        self.assertEqual(repr(Range(-2,-1)), "[-2--1)")
        self.assertEqual(repr(Range(-2,10)), "[-2-10)")

    def test_len(self):
        self.assertEqual(len(Range(0,10)), 10)
        self.assertEqual(len(Range(1,3)), 2)
        self.assertEqual(len(Range(-3,3)), 6)
        self.assertEqual(len(Range(-3,-3)), 0)

    def test_getitem(self):
        r = Range(-3, 10)
        self.assertEqual(r[0], -3)
        self.assertEqual(r[3], 0)
        self.assertEqual(r[12], 9)
        with self.assertRaises(IndexError):
            r[-1]
        with self.assertRaises(IndexError):
            r[13]

    def test_contains(self):
        r = Range(-3, 10)
        self.assertTrue(-3 in r)
        self.assertTrue(0 in r)
        self.assertTrue(9 in r)
        self.assertFalse(-4 in r)
        self.assertFalse(10 in r)

    def test_or(self):
        def tst_or(range1_start, range1_end, range2_start, range2_end, result_start, result_end):
            r1 = Range(range1_start, range1_end)
            r2 = Range(range2_start, range2_end)
            result = Range(result_start, result_end)
            self.assertEqual(r1|r2, result)
            self.assertEqual(r2|r1, result)
        tst_or(1,4, 4,8, 1,8)
        tst_or(1,4, 2,8, 1,8)
        with self.assertRaises(AssertionError):
            Range(1,4)|Range(4.00001, 5)

    def test_and(self):
        def tst_and(r1_start, r1_end, r2_start, r2_end, result_start, result_end):
            r1 = Range(r1_start, r1_end)
            r2 = Range(r2_start, r2_end)
            result = Range(result_start, result_end)
            self.assertEqual(r1&r2, result)
            self.assertEqual(r2&r1, result)

        tst_and(1,4, 4,8, 0,0)
        tst_and(1,4, 3,8, 3,4)
        tst_and(1,10, 2,4, 2,4)

    def test_eq(self):
        self.assertTrue(Range(1,4) == Range(1,4))
        self.assertFalse(Range(1,4) == Range(1,5))
        self.assertFalse(Range(1,4) == Range(2,4))
        self.assertTrue( Range(3,4) == (Range(1,4)&Range(3,8)))




################################################################################
class Grid:
    def __init__(self, rows):
        if len(rows) == 0: raise ValueError("Grid cannot hold an empty grid")
        if rows[0][-1] == "\n": raise ValueError("First line ends with a newline; you should strip those before passing to Grid")
        width = len(rows[0])
        self._rows = []
        for y, row in enumerate(rows):
            if len(row) != width: raise ValueError(f"Grid's input must be rectangular, but row 0 is {width} long but row {y} is {len(row)} long")
            self._rows.append(list(row))

    def height(self): return len(self._rows)
    def width(self): return len(self._rows[0])

    def row(self, idx):
        if idx < 0 or idx >= self.height():
            raise IndexError(f"row {idx} is out of range 0-{self.height()-1}")
        return self._rows[idx]

    def rows(self):
        return self._rows

    def column(self, idx):
        if idx < 0 or idx >= self.width():
            raise IndexError(f"column {idx} is out of range 0-{self.width()-1}")
        return list([row[idx] for row in self._rows])

    def columns(self):
        return [self.column(idx) for idx in range(self.width())]

    def _get_row_col(self, pos):
        if isinstance(pos, tuple):
            row = pos[1]
            col = pos[0]
        elif isinstance(pos, Pt):
            row = pos.y
            col = pos.x
        else:
            raise ValueError(f"Grid[x], takes a #,# tuple or a Pt, not a {type(pos)}")

        if row < 0 or row >= self.height():
            raise IndexError("Row/y value {row} is out of range 0-{self.height()-1}")
        if col < 0 or col >= self.width():
            raise IndexError("Column/x value {col} is out of range 0-{self.width()-1}")

        return row, col

    def __getitem__(self, pos):
        row, col = self._get_row_col(pos)
        return self._rows[row][col]

    def __setitem__(self, pos, value):
        row, col = self._get_row_col(pos)
        self._rows[row][col] = value

    def findall(self, value):
        result = []
        for y, row in enumerate(self.rows()):
            for x, cell in enumerate(row):
                if cell == value:
                    result.append(Pt(x,y))
        return result

    def copy(self):
        rows = []
        for row in self._rows:
            rows.append(row[:])
        return Grid(rows)

    def rotate_cw(self):
        rows = []
        for column in self.columns():
            rows.append(list(reversed(column)))
        self._rows = rows

    def rotate_ccw(self):
        self.rotate_cw()
        self.rotate_cw()
        self.rotate_cw()

    def rotate_180(self):
        self.rotate_cw()
        self.rotate_cw()

    def as_str_list(self, seperator = ""):
        rows = []
        for row in self._rows:
            rows.append(seperator.join(row))
        return rows

    def dump_as_str_list(self, file=sys.stdout, seperator = ""):
        for row in self.as_str_list():
            file.write(row+"\n")




class TestGrid(unittest.TestCase):
    def test_construction(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), [list("1234"), list("5678"), list("9ABC")])
        with self.assertRaises(ValueError):
            Grid([])
        with self.assertRaises(ValueError):
            Grid(["1234\n", "5678\n", "9ABC\n"])
        with self.assertRaises(ValueError):
            Grid(["1234", "567", "9ABC"])

    def test_height_width(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.width(), 4)
        self.assertEqual(g.height(), 3)

    def test_rows(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), [list("1234"), list("5678"), list("9ABC")])

    def test_columns(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.columns(), [list("159"), list("26A"), list("37B"), list("48C")])



    def test_row(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(1), list("5678"))
        with self.assertRaises(IndexError):
            g.row(-1)
        with self.assertRaises(IndexError):
            g.row(3)

    def test_column(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.column(1), list("26A"))
        with self.assertRaises(IndexError):
            g.column(-1)
        with self.assertRaises(IndexError):
            g.column(4)

    def test_getitem(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g[1,2], "A")
        self.assertEqual(g[Pt(1,2)], "A")

        with self.assertRaises(ValueError): g[9]

        with self.assertRaises(IndexError): g[-1,0]
        with self.assertRaises(IndexError): g[Pt(-1,0)]
        with self.assertRaises(IndexError): g[0,-1]
        with self.assertRaises(IndexError): g[Pt(0,-1)]

        with self.assertRaises(IndexError): g[0,3]
        with self.assertRaises(IndexError): g[Pt(0,3)]
        with self.assertRaises(IndexError): g[4,0]
        with self.assertRaises(IndexError): g[Pt(4,0)]

    def test_setitem(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(0), list("1234"))
        self.assertEqual(g.row(1), list("5678"))
        self.assertEqual(g.row(2), list("9ABC"))
        g[Pt(0,0)] = "X"
        g[1,1] = "Y"
        g[3,2] = "Z"
        self.assertEqual(g.row(0), list("X234"))
        self.assertEqual(g.row(1), list("5Y78"))
        self.assertEqual(g.row(2), list("9ABZ"))

        with self.assertRaises(ValueError): g[9] = "a"

        with self.assertRaises(IndexError): g[-1,0] = "a"
        with self.assertRaises(IndexError): g[Pt(-1,0)] = "a"
        with self.assertRaises(IndexError): g[0,-1] = "a"
        with self.assertRaises(IndexError): g[Pt(0,-1)] = "a"

        with self.assertRaises(IndexError): g[0,3] = "a"
        with self.assertRaises(IndexError): g[Pt(0,3)] = "a"
        with self.assertRaises(IndexError): g[4,0] = "a"
        with self.assertRaises(IndexError): g[Pt(4,0)] = "a"

    def test_findall(self):
        g = Grid(["..#.",
                     "#..#",
                     "....",
                     ".#..",])
        hits = set(g.findall("#"))
        expected = {
                Pt(2,0),
                Pt(0,1),
                Pt(3,1),
                Pt(1,3),
                }
        self.assertEqual(hits, expected)

        hits = g.findall("X")
        self.assertEqual(len(hits), 0)

    def test_copy(self):
        g1 = Grid(["123", "456"])
        self.assertEqual(g1.row(0), list("123"))
        self.assertEqual(g1.row(1), list("456"))
        g2 = g1.copy()
        self.assertEqual(g2.row(0), list("123"))
        self.assertEqual(g2.row(1), list("456"))
        g1[0,0] = "X"
        g2[1,1] = "Y"
        self.assertEqual(g1.row(0), list("X23"))
        self.assertEqual(g1.row(1), list("456"))
        self.assertEqual(g2.row(0), list("123"))
        self.assertEqual(g2.row(1), list("4Y6"))
        
    def test_rotate_cw(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(0), list("1234"))
        self.assertEqual(g.row(1), list("5678"))
        self.assertEqual(g.row(2), list("9ABC"))
        g.rotate_cw()
        self.assertEqual(g.row(0), list("951"))
        self.assertEqual(g.row(1), list("A62"))
        self.assertEqual(g.row(2), list("B73"))
        self.assertEqual(g.row(3), list("C84"))

    def test_rotate_ccw(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.rotate_ccw()
        self.assertEqual(g.row(0), list("48C"))
        self.assertEqual(g.row(1), list("37B"))
        self.assertEqual(g.row(2), list("26A"))
        self.assertEqual(g.row(3), list("159"))

    def test_rotate_180(self):
        g = Grid(["1234", "5678", "9ABC"])
        g.rotate_180()
        self.assertEqual(g.row(0), list("CBA9"))
        self.assertEqual(g.row(1), list("8765"))
        self.assertEqual(g.row(2), list("4321"))

    def test_as_str_list(self):
        g = Grid(["1234", "5678", "9ABC"])
        self.assertEqual(g.as_str_list(), ["1234", "5678", "9ABC"])

    def test_dump_as_str_list(self):
        g = Grid(["1234", "5678", "9ABC"])
        import io
        out = io.StringIO()
        g.dump_as_str_list(out)
        self.assertEqual(out.getvalue(), "1234\n5678\n9ABC\n")




################################################################################
class StrGrid (Grid):
    def __init__(self, rows):

        if len(rows) > 0 and len(rows[0]) > 0 and rows[0][-1] == "\n":
            raise ValueError("First line ends with a newline; you should strip those before passing to StrGrid")
        for row in rows:
            if not isinstance(row, str):
                raise ValueError(f"Individual rows must be strings, not {type(row)}")
        super().__init__(rows)

    def row(self, idx):
        return "".join(super().row(idx))

    def rows(self):
        return ["".join(x) for x in self._rows]

    def column(self, idx):
        return "".join(super().column(idx))

    def __setitem__(self, pos, value):
        if not isinstance(value, str):
            raise ValueError(f"Can only add a string of length 1, not a {type(value)}")
        if len(value) != 1:
            raise ValueError(f"Can only add a string of length 1, not length {len(value)}")
        super().__setitem__(pos, value)

    def copy(self):
        rows = []
        for row in self.rows():
            rows.append(row)
        return StrGrid(rows)

    def rotate_cw(self):
        raise  NotImplementedError("rotate_cw is not available in StrGrid. Maybe use Grid?")

    def rotate_ccw(self):
        raise  NotImplementedError("rotate_ccw is not available in StrGrid. Maybe use Grid?")

    def rotate_180(self):
        raise  NotImplementedError("rotate_180 is not available in StrGrid. Maybe use Grid?")

    def as_str_list(self, seperator = ""):
        return self.rows()


class TestStrGrid(unittest.TestCase):
    def test_construction(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), ["1234", "5678", "9ABC"])
        with self.assertRaises(ValueError):
            StrGrid([])
        with self.assertRaises(ValueError):
            StrGrid(["1234\n", "5678\n", "9ABC\n"])
        with self.assertRaises(ValueError):
            StrGrid(["1234", "567", "9ABC"])
        with self.assertRaises(ValueError):
            StrGrid(["12", ["3","4"]])

    def test_height_width(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.width(), 4)
        self.assertEqual(g.height(), 3)

    def test_rows(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.rows(), ["1234", "5678", "9ABC"])

    def test_columns(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.columns(), ["159", "26A", "37B", "48C"])

    def test_row(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(1), "5678")
        with self.assertRaises(IndexError):
            g.row(-1)
        with self.assertRaises(IndexError):
            g.row(3)

    def test_column(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.column(1), "26A")
        with self.assertRaises(IndexError):
            g.column(-1)
        with self.assertRaises(IndexError):
            g.column(4)

    def test_getitem(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g[1,2], "A")
        self.assertEqual(g[Pt(1,2)], "A")

        with self.assertRaises(ValueError): g[9]

        with self.assertRaises(IndexError): g[-1,0]
        with self.assertRaises(IndexError): g[Pt(-1,0)]
        with self.assertRaises(IndexError): g[0,-1]
        with self.assertRaises(IndexError): g[Pt(0,-1)]

        with self.assertRaises(IndexError): g[0,3]
        with self.assertRaises(IndexError): g[Pt(0,3)]
        with self.assertRaises(IndexError): g[4,0]
        with self.assertRaises(IndexError): g[Pt(4,0)]

    def test_setitem(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.row(0), "1234")
        self.assertEqual(g.row(1), "5678")
        self.assertEqual(g.row(2), "9ABC")
        g[Pt(0,0)] = "X"
        g[1,1] = "Y"
        g[3,2] = "Z"
        self.assertEqual(g.row(0), "X234")
        self.assertEqual(g.row(1), "5Y78")
        self.assertEqual(g.row(2), "9ABZ")

        with self.assertRaises(ValueError): g[0,0] = "aa"
        with self.assertRaises(ValueError): g[0,0] = ""
        with self.assertRaises(ValueError): g[0,0] = 1
        with self.assertRaises(ValueError): g[9] = "a"

        with self.assertRaises(IndexError): g[-1,0] = "a"
        with self.assertRaises(IndexError): g[Pt(-1,0)] = "a"
        with self.assertRaises(IndexError): g[0,-1] = "a"
        with self.assertRaises(IndexError): g[Pt(0,-1)] = "a"

        with self.assertRaises(IndexError): g[0,3] = "a"
        with self.assertRaises(IndexError): g[Pt(0,3)] = "a"
        with self.assertRaises(IndexError): g[4,0] = "a"
        with self.assertRaises(IndexError): g[Pt(4,0)] = "a"

    def test_findall(self):
        g = StrGrid(["..#.",
                     "#..#",
                     "....",
                     ".#..",])
        hits = set(g.findall("#"))
        expected = {
                Pt(2,0),
                Pt(0,1),
                Pt(3,1),
                Pt(1,3),
                }
        self.assertEqual(hits, expected)

        hits = g.findall("X")
        self.assertEqual(len(hits), 0)

    def test_copy(self):
        g1 = StrGrid(["123", "456"])
        self.assertEqual(g1.row(0), "123")
        self.assertEqual(g1.row(1), "456")
        g2 = g1.copy()
        self.assertEqual(g2.row(0), "123")
        self.assertEqual(g2.row(1), "456")
        g1[0,0] = "X"
        g2[1,1] = "Y"
        self.assertEqual(g1.row(0), "X23")
        self.assertEqual(g1.row(1), "456")
        self.assertEqual(g2.row(0), "123")
        self.assertEqual(g2.row(1), "4Y6")

    def test_as_str_list(self):
        g = StrGrid(["1234", "5678", "9ABC"])
        self.assertEqual(g.as_str_list(), ["1234", "5678", "9ABC"])


################################################################################
if __name__ == '__main__':
    unittest.main()
