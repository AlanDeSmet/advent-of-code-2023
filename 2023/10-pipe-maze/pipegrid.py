#! /usr/bin/python3

from dataclasses import dataclass

@dataclass
class Pt:
    x: int
    y: int

    def __add__(self, other): return Pt(self.x+other.x, self.y+other.y)
    def __neg__(self): return Pt(-self.x, -self.y)
    def __eq__(self, other): return self.y == other.y and self.x == other.x
    def __lt__(self, other):
        if self.y != other.y:
            return self.y < other.y
        return self.x < other.x
    def __repr__(self): return f"({self.x},{self.y})"
    def __hash__(self): return self.x + self.y*10000000

NORTH = Pt( 0, -1)
SOUTH = Pt( 0, +1)
WEST  = Pt(-1,  0)
EAST  = Pt(+1,  0)
DIRECTIONS = (NORTH, SOUTH, WEST, EAST)

PIPES = {
        "|": sorted((NORTH, SOUTH)),
        "-": sorted((EAST,  WEST)),
        "L": sorted((NORTH, EAST)),
        "J": sorted((NORTH, WEST)),
        "7": sorted((SOUTH, WEST)),
        "F": sorted((SOUTH, EAST)),
        ".": (),
        }

class PipeGrid:
    def __init__(self, rows):
        self.rows = list([list(x.rstrip()) for x in rows])
        self.start = self._find_start()
        self.rows[self.start.y][self.start.x] = self._identify_start_pipe()
        self.on_start_loop = self._find_start_loop_positions()

    def _find_start(self):
        for y, row in enumerate(self.rows):
            if "S" in row:
                x = row.index("S")
                return Pt(x,y)

    def connects(self, position, direction):
        if position.y < 0 or position.y > len(self.rows):
            return False
        if position.x < 0 or position.x > len(self.rows[0]):
            return False
        dirs = self.exits(position)
        return direction in dirs

    def _identify_start_pipe(self):
        connections = []
        for direction in (NORTH, SOUTH, EAST, WEST):
            if self.connects(self.start + direction, -direction):
                connections.append(direction)
        assert len(connections) == 2
        connections.sort()
        for pipe, directions in PIPES.items():
            if directions == connections:
                return pipe
        assert False

    def exits(self, pos):
        return PIPES[self.rows[pos.y][pos.x]]

    def connected(self, pos, dir):
        exits = self.exits(pos)
        return dir in exits

    def continue_direction(self, pos, last_dir):
        exits = self.exits(pos)
        if (-last_dir) == exits[0]:
            return exits[1]
        return exits[0]

    def _find_start_loop_positions(self):
        positions = set()
        entered_dir = self.exits(self.start)[0]
        pos = self.start + entered_dir
        positions.add(pos)
        while pos != self.start:
            entered_dir = self.continue_direction(pos, entered_dir)
            pos += entered_dir
            positions.add(pos)
        return positions

    def length_of_start_pipe(self):
        return len(self.on_start_loop)


