#! /usr/bin/python3

import advent
import re
from collections import defaultdict

def revstr(s):
    return "".join(reversed(s))

def extract_column(rows, column):
    return "".join([row[column] for row in rows])

class Tile:
    def __init__(self, rows):
        self.id = int(re.search(r'\d+', rows.pop(0)).group(0))
        self.rows = rows

    def top(self): return self.rows[0]
    def bottom(self): return self.rows[-1]
    def left(self): return extract_column(self.rows, 0)
    def right(self): return extract_column(self.rows, -1)

    def top_uid(self): return max(self.top(), revstr(self.top()))
    def bottom_uid(self): return max(self.bottom(), revstr(self.bottom()))
    def left_uid(self): return max(self.left(), revstr(self.left()))
    def right_uid(self): return max(self.right(), revstr(self.right()))

    def rotate_90_cw(self):
        newrows = []
        for i in range(len(self.rows)):
            newrows.append(revstr(extract_column(self.rows, i)))
        self.rows = newrows

    def flip_around_horiz(self):
        self.rows = list(reversed(self.rows))

    def __repr__(self):
        return "\n".join(self.rows)

    def rotate_and_flip_to_match_top_edge(self, edge):
        self.rotate_and_flip_to_match_left_edge(revstr(edge))
        self.rotate_90_cw()

    def rotate_and_flip_to_match_left_edge(self, edge):
        for i in range(4):
            if self.left() == edge: 
                #print(f"{self.left()} == {edge} base\n{self}")
                return
            self.flip_around_horiz()
            if self.left() == edge:
                #print(f"{self.left()} == {edge} flipped\n{self}")
                return
            self.flip_around_horiz()
            self.rotate_90_cw()
        assert False, f"\n{self.id}\n{self}"





files = advent.initialize(2020,20,1,"Jurassic Jigsaw", ["output"])

# Load tiles
tile_strs = files.input.read().rstrip().split("\n\n")
tiles = []
shared_edges = defaultdict(list)
for tile_str in tile_strs:
    rows = tile_str.split("\n")
    tile = Tile(rows)
    tiles.append(tile)

# Find shared edges
for tile in tiles:
    shared_edges[tile.top_uid()].append(tile)
    shared_edges[tile.bottom_uid()].append(tile)
    shared_edges[tile.left_uid()].append(tile)
    shared_edges[tile.right_uid()].append(tile)

# find a corner to start at
corner = None
for tile in tiles:
    pairings = (
        len(shared_edges[tile.top_uid()]) + 
        len(shared_edges[tile.bottom_uid()]) + 
        len(shared_edges[tile.left_uid()]) + 
        len(shared_edges[tile.right_uid()])
        )
    if pairings == 6:
        corner = tile
        break

# Rotate that corner until it's the upper left corner
while ((len(shared_edges[corner.top_uid()]) != 1) or
        (len(shared_edges[corner.left_uid()]) != 1)):
    print("rotate once")
    corner.rotate_90_cw()

# Go down from upper-left corner
result = [ [corner] ]
consider = corner
#print(corner.id)
#print(corner)
while len(shared_edges[consider.bottom_uid()]) == 2:
    pieces = shared_edges[consider.bottom_uid()]
    if pieces[0] == consider: newpiece = pieces[1]
    else: newpiece = pieces[0]
    newpiece.rotate_and_flip_to_match_top_edge(consider.bottom())
    result.append( [newpiece] )
    assert consider.bottom() == newpiece.top(), f"{consider.bottom()} != {newpiece.top()}:\n{newpiece}"
    #print(newpiece.top_uid())
    consider = newpiece
    #print(consider.id)
    #print(consider)


# Go right from left edge
for row in result:
    consider = row[0]
    while len(shared_edges[consider.right_uid()]) == 2:
        pieces = shared_edges[consider.right_uid()]
        if pieces[0] == consider: newpiece = pieces[1]
        else: newpiece = pieces[0]
        newpiece.rotate_and_flip_to_match_left_edge(consider.right())
        row.append(newpiece)
        assert consider.right() == newpiece.left()
        consider = newpiece
    
# Assemble it
lines = []
for row in result:
    height = len(row[0].rows)
    for y in range(1, height-1):
        out = ""
        for item in row:
            out += item.rows[y][1:-1]
        lines.append(out)

# Dump it so we can compare to truth
# Hackily flip it so it matches the official
tmptile = Tile(["0"]+ lines)
tmptile.flip_around_horiz()
tmptile.rotate_90_cw()
for row in tmptile.rows:
    files.output.write(row+"\n")

#  --▲
SEA_MONSTER_RIGHT_UP = [
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   ",
    ]

#  --▼
SEA_MONSTER_RIGHT_DOWN = list(reversed(SEA_MONSTER_RIGHT_UP))

#  ▲--
SEA_MONSTER_LEFT_UP = list([revstr(row) for row in SEA_MONSTER_RIGHT_UP])

#  ▼--
SEA_MONSTER_LEFT_DOWN = list([revstr(row) for row in SEA_MONSTER_RIGHT_DOWN])

# |
# ▶
SEA_MONSTER_DOWN_RIGHT = []
for i in range(len(SEA_MONSTER_RIGHT_UP[0])):
    SEA_MONSTER_DOWN_RIGHT.append(revstr(extract_column(SEA_MONSTER_RIGHT_UP, i)))

# ▶
# |
SEA_MONSTER_UP_RIGHT = list(reversed(SEA_MONSTER_DOWN_RIGHT))


# |
# ◀
SEA_MONSTER_DOWN_LEFT = list([revstr(row) for row in SEA_MONSTER_DOWN_RIGHT])

# ◀
# |
SEA_MONSTER_UP_LEFT = list([revstr(row) for row in SEA_MONSTER_UP_RIGHT])

SEA_MONSTERS = [
        SEA_MONSTER_RIGHT_UP,
        SEA_MONSTER_RIGHT_DOWN,
        SEA_MONSTER_LEFT_UP,
        SEA_MONSTER_LEFT_DOWN,
        SEA_MONSTER_DOWN_RIGHT,
        SEA_MONSTER_UP_RIGHT,
        SEA_MONSTER_DOWN_LEFT,
        SEA_MONSTER_UP_LEFT,
        ]

#for monster in SEA_MONSTERS:
#    for row in monster:
#        print(row)
#    print("--------")


# Search for Nessie
monster_cells = set()
for monster in SEA_MONSTERS:
    for y in range(0,len(lines)-len(monster)+1):
        for x in range(0,len(lines[0])-len(monster[0])+1):
            # Like all good cyptids, assume you saw it until proven otherwise
            found = True 
            possible_locations = set()
            for mon_y in range(len(monster)):
                for mon_x in range(len(monster[0])):
                    if monster[mon_y][mon_x] == " ":
                        continue
                    assert monster[mon_y][mon_x] == "#"
                    if lines[y+mon_y][x+mon_x] != "#":
                        found = False
                        break
                    possible_locations.add( (y+mon_y, x+mon_x) )
            if found:
                print(possible_locations)
                monster_cells |= possible_locations
                print("hit")




# Dump it
import sys
for y in range(0,len(lines)):
    for x in range(0,len(lines[0])):
        if (y,x) in monster_cells:
            assert lines[y][x] == "#"
            sys.stdout.write("█")
        else:
            sys.stdout.write(lines[y][x])

    sys.stdout.write("\n")


# Count roughness
count = 0
for y, line in enumerate(lines):
    for x, cell in enumerate(line):
        if cell == "#" and (y,x) not in monster_cells:
            count += 1
files.result.write(f"{count}\n")






