#! /usr/bin/python3

class Hill:
    def __init__(self, rows):
        first_len = len(rows[0])
        for row in rows: assert first_len == len(row)

        self.rows = rows

    def width(self):
        return len(self.rows[0])

    def height(self):
        return len(self.rows)

    def is_tree(self, column, row):
        column %= self.width()
        return self.rows[row][column]

    def __str__(self):
        out = ""
        for row in self.rows:
            outrow = ""
            for cell in row:
                if cell:
                    outrow += "#"
                else:
                    outrow += "."
            out += outrow + "\n"
        return out

def load_hill(file):
    rows = []
    for line in file:
        line = line.rstrip()
        rows.append(list([True if cell=="#" else False for cell in line]))
    return Hill(rows)

def count_hits(hill, slopex, slopey):
    posx = 0
    posy = 0

    tree_hits = 0
    while (posy+slopey) < hill.height():
        posx += slopex
        posy += slopey
        tree_hits += hill.is_tree(posx, posy)
    return tree_hits
