#! /usr/bin/python3

class SubBag:
    def __init__(self, count, description):
        self.count = count
        self.description = description

    def __str__(self):
        return f"{self.count} {self.description}"

    def __repr__(self):
        return f"{self.count} {self.description}"

def load_bag_rules(file):
    rules = {}
    for line in file:
        line = line.rstrip()
        parent, children = line.split(" bags contain ")
        assert parent not in rules, f"Duplicate definition of {parent}"
        if children == "no other bags.":
            rules[parent] = []
            continue
        children.rstrip(".")
        children = children.split(", ")
        child_records = []
        for child in children:
            count, mod, color, bags = child.split(" ")
            child_records.append(SubBag(int(count), f"{mod} {color}"))
        rules[parent] = child_records
    return rules




def main():
    rules = load_bag_rules(open("test.input"))
    print(rules)





if __name__ == '__main__':
    import sys
    sys.exit(main())
