#! /usr/bin/python3

import sys
import struct

register_offset = 32768

def load_binary(filename):
    with open(filename, "rb") as f:
        raw = f.read()
    memory = []
    for value in struct.iter_unpack("<H", raw):
        assert len(value) == 1
        value = value[0]
        assert value < 32776
        memory.append(value)
    return memory

def disassemble(memory, pc):
    def read_arg(num):
        assert (pc+num) < len(memory)
        val = memory[pc+num]
        if val < register_offset:
            return str(val)
        return f"r{val-register_offset}"

    def write_arg(num):
        assert (pc+num) < len(memory)
        val = memory[pc+num]
        assert val >= register_offset
        return f"r{val-register_offset}"

    opcode = memory[pc]
    match memory[pc]:
        case 0:  return "halt"
        case 1:  return f"set  {write_arg(1)} = {read_arg(2)}"
        case 2:  return f"push {read_arg(1)}"
        case 3:  return f"pop  {write_arg(1)}"
        case 4:  return f"eq   {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 5:  return f"gt   {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 6:  return f"jmp  {read_arg(1)}"
        case 7:  return f"jt   {read_arg(1)} {read_arg(2)}"
        case 8:  return f"jf   {read_arg(1)} {read_arg(2)}"
        case 9:  return f"add  {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 10: return f"mult {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 11: return f"mod  {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 12: return f"and  {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 13: return f"or   {write_arg(1)} {read_arg(2)} {read_arg(3)}"
        case 14: return f"not  {write_arg(1)} {read_arg(2)}"
        case 15: return f"rmem {write_arg(1)} {read_arg(2)}"
        case 16: return f"wmem {read_arg(1)} {read_arg(2)}"
        case 17: return f"call {read_arg(1)}"
        case 18: return f"ret"
        case 19: return f"out  {read_arg(1)}"
        case 20: return f"in   {write_arg(1)}"
        case 21: return f"noop"
        case _:
            assert False

def interpret(memory):
    pc = 0
    registers = [0]*8
    stack = []

    def read_arg(num):
        assert (pc+num) < len(memory)
        val = memory[pc+num]
        if val >= register_offset:
            reg_idx = val - register_offset
            val = registers[reg_idx]
        return val

    def write_arg(num):
        assert (pc+num) < len(memory)
        val = memory[pc+num]
        assert val >= register_offset
        return val - register_offset


    MODULUS = 32768

    while True:
        assert pc < len(memory)
        opcode = memory[pc]
        #print(pc, disassemble(memory, pc))
        assert opcode >= 0 and opcode <= 21
        match memory[pc]:
            case 0: # halt
                return
            case 1: # set a b
                registers[write_arg(1)] = read_arg(2)
                pc += 3
            case 2: # push a
                stack.append(read_arg(1))
                pc += 2
            case 3: # pop a
                registers[write_arg(1)] = stack.pop()
                pc += 2
            case 4: # eq a b c
                registers[write_arg(1)] = int(read_arg(2) == read_arg(3))
                pc += 4
            case 5: # gt a b c
                registers[write_arg(1)] = int(read_arg(2) > read_arg(3))
                pc += 4
            case 6: # jmp a
                pc = read_arg(1)
            case 7: # jt a b
                if read_arg(1):
                    pc = read_arg(2)
                else:
                    pc += 3
            case 8: # jf a b
                if not read_arg(1):
                    pc = read_arg(2)
                else:
                    pc += 3
            case 9: # add a b c
                registers[write_arg(1)] = (read_arg(2) + read_arg(3)) % MODULUS
                pc += 4
            case 10: # mult a b c
                registers[write_arg(1)] = (read_arg(2) * read_arg(3)) % MODULUS
                pc += 4
            case 11: # mod a b c
                registers[write_arg(1)] = read_arg(2) % read_arg(3)
                pc += 4
            case 12: # and a b c
                registers[write_arg(1)] = read_arg(2) & read_arg(3)
                pc += 4
            case 13: # or a b c
                registers[write_arg(1)] = read_arg(2) | read_arg(3)
                pc += 4
            case 14: # not a b
                registers[write_arg(1)] = read_arg(2) ^ 0x7FFF
                pc += 3
            case 15: # rmem a b
                registers[write_arg(1)] = memory[read_arg(2)]
                pc += 3
            case 16: # wmem a b
                memory[read_arg(1)] = read_arg(2)
                pc += 3
            case 17: # call a
                stack.append(pc+2)
                #print("Call jumping")
                #print("from", pc)
                pc = read_arg(1)
                #print("to", pc)
            case 18: # ret
                pc = stack.pop()
            case 19: # out a
                #print("out")
                sys.stdout.write( chr(read_arg(1)) )
                pc += 2
            case 20: # in a
                registers[write_arg(1)] = ord(sys.stdin.read(1))
                #print("READ", chr(registers[write_arg(1)]), registers[write_arg(1)])
                pc += 2
            case 21: # noop
                #print("noop")
                pc += 1
            case _:
                assert False


def main():
    memory = load_binary("challenge.bin")
    interpret(memory)




    return 0


if __name__ == '__main__':
    sys.exit(main())
