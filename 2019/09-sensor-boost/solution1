#! /usr/bin/python3

import advent
from itertools import permutations
from enum import Enum

files = advent.initialize(2023,7,3,"Sensor Boost")

class SimState(Enum):
    READY = 0
    HALTED = 1
    BLOCKED_ON_INPUT = 2

sim_counter = 0
class IntcodeSimulator:
    def __init__(self, memory, name=None, inputs=[]):
        self.inputs = inputs
        global sim_counter
        sim_counter += 1
        if name is None:
            name = f"unnamed {sim_counter}"
        self.name = name
        self.outputs = []
        self.memory = memory[:]
        self.pc = 0
        self.state = SimState.READY
        self.rbase = 0

    def run(self):
        assert self.state != SimState.HALTED, "Attemping to run simulator that halted"
        if self.state == SimState.BLOCKED_ON_INPUT:
            # See if we can unblock
            self.step()
        while self.state == SimState.READY:
            self.step()

    def step(self):
            #print(pc, mem[pc:pc+4])
            mem = self.memory
            pc = self.pc

            raw_p = mem[pc+1:pc+4]

            mode = [0]*10
            mode[0] = (mem[pc] // 100) % 10
            mode[1] = (mem[pc] // 1000) % 10
            mode[2] = (mem[pc] // 10000) % 10
            opcode = mem[pc] % 100

            p = [0]*10
            for idx, raw_param in enumerate(raw_p):
                if mode[idx] == 0:
                    if len(mem) > raw_param:
                        p[idx] = mem[raw_param]
                    else:
                        p[idx] = 0
                elif mode[idx] == 1:
                    p[idx] = raw_param
                elif mode[idx] == 2:
                    if len(mem) > (raw_param+self.rbase):
                        p[idx] = mem[raw_param+self.rbase]
                    else:
                        p[idx] = 0
                    raw_p[idx] += self.rbase
                else:
                    assert False, f"Unknown parameter mode {mode[idx]}"

            if self.state == SimState.BLOCKED_ON_INPUT:
                assert opcode == 3, f"State is blocked on input, but opcode is {opcode} instead of 3"

            match opcode:
                case 1: # Add
                    assert mode[2] != 1
                    mem[raw_p[2]] = p[1] + p[0]
                    #print(f"Add {p[0]} to {p[1]} to @{p[2]}: {mem[raw_p[2]]}")
                    #print("",mem)
                    pc += 4

                case 2: # Multiply
                    assert mode[2] != 1
                    mem[raw_p[2]] = p[1] * p[0]
                    #print(f"Mul {p[0]} to {p[1]} to @{p[2]}: {mem[raw_p[2]]}")
                    #print("",mem)
                    pc += 4

                case 3: # Input
                    if len(self.inputs) == 0:
                        #if not self.state == SimState.BLOCKED_ON_INPUT:
                        #    print(f"{self.name} blocking on input")
                        self.state = SimState.BLOCKED_ON_INPUT
                        return
                    self.state = SimState.READY
                    assert mode[0] != 1
                    mem[raw_p[0]] = self.inputs.pop(0)
                    #print(f"Input {mem[raw_p[0]]}")
                    #print("",mem)
                    pc += 2

                case 4: # Output
                    #print(f"Output {p[0]}")
                    #print("",mem)
                    self.outputs.append(p[0])
                    pc += 2

                case 5: # Jump If True
                    if p[0]: pc = p[1]
                    else: pc += 3

                case 6: # Jump If False
                    if not p[0]: pc = p[1]
                    else: pc += 3

                case 7: # Less Than
                    val = 0
                    if p[0] < p[1]:
                        val = 1
                    mem[raw_p[2]] = val
                    pc += 4

                case 8: # Equals
                    val = 0
                    if p[0] == p[1]:
                        val = 1
                    mem[raw_p[2]] = val
                    pc += 4

                case 9: # Change Relative Base
                    self.rbase += p[0]
                    pc += 2

                case 99: # Halt
                    #print(f"Halting {self.name}")
                    #print("",mem)
                    self.state = SimState.HALTED
                    # pc += 1 # Hypothetically
                case _: #
                    assert False, f"Unknown opcode {mem[pc]} at PC {pc}"
            self.pc = pc

    def halted(self): return self.state == SimState.HALTED
    def ready(self): return self.state == SimState.READY

#def try_combination(memory, settings):
#    sim1 = IntcodeSimulator(memory, "sim1", [settings.pop(0),0])
#    sim2 = IntcodeSimulator(memory, "sim2", sim1.outputs)
#    sim2.inputs.append(settings.pop(0))
#    sim3 = IntcodeSimulator(memory, "sim3", sim2.outputs)
#    sim3.inputs.append(settings.pop(0))
#    sim4 = IntcodeSimulator(memory, "sim4", sim3.outputs)
#    sim4.inputs.append(settings.pop(0))
#    sim5 = IntcodeSimulator(memory, "sim5", sim4.outputs)
#    sim5.inputs.append(settings.pop(0))
#    sim5.outputs = sim1.inputs
#
#    while not sim5.halted():
#        #print("loop", sim5.state, sim4.state, sim3.state, sim2.state, sim1.state)
#        #print(sim1.outputs, sim2.outputs, sim3.outputs, sim4.outputs, sim5.outputs)
#        # Intentionally running these backwar to force
#        # input blocking to test it.
#        sim5.run()
#        if not sim4.halted(): sim4.run()
#        if not sim3.halted(): sim3.run()
#        if not sim2.halted(): sim2.run()
#        if not sim1.halted(): sim1.run()
#
#    return sim5.outputs.pop(0)
#
#def try_all_combinations(memory):
#    best_score = 0
#    for permutation in permutations([5,6,7,8,9]):
#        score = try_combination(memory, list(permutation))
#        best_score = max(best_score, score)
#    return best_score

memory = list([int(x) for x in files.input.sreadline().split(",")]) + [0]*10000

sim = IntcodeSimulator(memory, inputs=[1])
sim.run()
print("Part 1", sim.outputs)

sim = IntcodeSimulator(memory, inputs=[2])
sim.run()
print("Part 2", sim.outputs)


