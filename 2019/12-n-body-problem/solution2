#! /usr/bin/python3

import advent
from advent import Pt3, least_common_multiple
import re
import itertools

def fmtpt3(p):
    return f"<x={p.x:2d}, y={p.y:2d}, z={p.z:2d}>"

class Moon:
    def __init__(self, pos, vel=Pt3(0,0,0)):
        self.pos = pos
        self.vel = vel

    def __repr__(self):
        return f"pos={fmtpt3(self.pos)}, vel={fmtpt3(self.vel)}"

    def __eq__(self, other):
        return self.pos == other.pos and self.vel == other.vel

    def copy(self):
        return Moon(self.pos, self.vel)

def load_moon_from_line(line):
    m = re.match(r'<x=(-?\d+), y=(-?\d+), z=(-?\d+)>$', line)
    assert m is not None
    return Moon(
            Pt3(int(m.group(1)),
                int(m.group(2)),
                int(m.group(3))
                )
            )

def calc_shift(a,b):
    if a > b: return -1, +1
    if a < b: return +1, -1
    return 0,0

def report_moons(stepnum, moons, file):
    s = "" if stepnum==1 else "s"
    file.print(f"After {stepnum} step{s}:")
    for moon in moons:
        file.print(moon)
    file.print()

def update_moons_velocities(moons):
    for a, b in itertools.combinations(moons,2):
        a_shift_x, b_shift_x = calc_shift(a.pos.x, b.pos.x)
        a_shift_y, b_shift_y = calc_shift(a.pos.y, b.pos.y)
        a_shift_z, b_shift_z = calc_shift(a.pos.z, b.pos.z)

        a.vel += Pt3(a_shift_x, a_shift_y, a_shift_z)
        b.vel += Pt3(b_shift_x, b_shift_y, b_shift_z)

def update_moons_positions(moons):
    for moon in moons:
        moon.pos += moon.vel

def calculate_energy(stepnum, moons, file):
    file.print(f"Energy after {stepnum} steps:")
    subtotals = []
    for moon in moons:
        outline = ""

        x = abs(moon.pos.x)
        y = abs(moon.pos.y)
        z = abs(moon.pos.z)
        potential = x+y+z
        outline += f"pot: {x:2d} + {y:2d} + {z:2d} = {potential:2d};   "

        x = abs(moon.vel.x)
        y = abs(moon.vel.y)
        z = abs(moon.vel.z)
        kinetic = x+y+z
        outline += f"kin: {x:2d} + {y:2d} + {z:2d} = {kinetic:2d};   "

        subtotal = potential * kinetic
        outline += f"total: {potential:2d} * {kinetic:2d} = {subtotal}"
        subtotals.append(subtotal)
        file.print(outline)
    total = sum(subtotals)
    subtotals_str = ' + '.join([str(x) for x in subtotals])
    file.print(f"Sum of total energy: {subtotals_str} = {total}")
    return total


def do_moons_match_original(moons, original_moons, variable):
    for moon, original in zip(moons, original_moons):
        if getattr(moon.pos, variable) != getattr(original.pos, variable):
            return False
        if getattr(moon.vel, variable) != getattr(original.vel, variable):
            return False
    return True



files = advent.initialize(2023,12,1,"The N-Body Problem",["output"])

moons = []
for line in files.input.sreadlines():
    moons.append(load_moon_from_line(line))

original_moons = []
for moon in moons:
    original_moons.append(moon.copy())

report_moons(0, moons, files.output)

found_x = None
found_y = None
found_z = None

stepnum = 0
while not found_x or not found_y or not found_z:
    stepnum += 1
    update_moons_velocities(moons)
    update_moons_positions(moons)
    if not found_x and do_moons_match_original(moons, original_moons, 'x'):
        print(f"x looped after {stepnum}")
        found_x = stepnum
    if not found_y and do_moons_match_original(moons, original_moons, 'y'):
        print(f"y looped after {stepnum}")
        found_y = stepnum
    if not found_z and do_moons_match_original(moons, original_moons, 'z'):
        print(f"z looped after {stepnum}")
        found_z = stepnum

print(least_common_multiple(found_x, found_y, found_z))




