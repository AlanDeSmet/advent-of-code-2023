#! /usr/bin/python3

import advent
from advent import Pt

from enum import Enum
class SimState(Enum):
    READY = 0
    HALTED = 1
    BLOCKED_ON_INPUT = 2

sim_counter = 0
class IntcodeSimulator:
    def __init__(self, memory, name=None, inputs=None):
        if inputs is None:
            inputs = []
        self.inputs = inputs
        global sim_counter
        sim_counter += 1
        if name is None:
            name = f"unnamed {sim_counter}"
        self.name = name
        self.outputs = []
        self.memory = memory[:]
        self.pc = 0
        self.state = SimState.READY
        self.rbase = 0

    def run(self):
        assert self.state != SimState.HALTED, "Attemping to run simulator that halted"
        if self.state == SimState.BLOCKED_ON_INPUT:
            # See if we can unblock
            self.step()
        while self.state == SimState.READY:
            self.step()

    def step(self):
            #print(pc, mem[pc:pc+4])
            mem = self.memory
            pc = self.pc

            raw_p = mem[pc+1:pc+4]

            mode = [0]*10
            mode[0] = (mem[pc] // 100) % 10
            mode[1] = (mem[pc] // 1000) % 10
            mode[2] = (mem[pc] // 10000) % 10
            opcode = mem[pc] % 100

            p = [0]*10
            for idx, raw_param in enumerate(raw_p):
                if mode[idx] == 0:
                    if len(mem) > raw_param:
                        p[idx] = mem[raw_param]
                    else:
                        p[idx] = 0
                elif mode[idx] == 1:
                    p[idx] = raw_param
                elif mode[idx] == 2:
                    if len(mem) > (raw_param+self.rbase):
                        p[idx] = mem[raw_param+self.rbase]
                    else:
                        p[idx] = 0
                    raw_p[idx] += self.rbase
                else:
                    assert False, f"Unknown parameter mode {mode[idx]}"

            if self.state == SimState.BLOCKED_ON_INPUT:
                assert opcode == 3, f"State is blocked on input, but opcode is {opcode} instead of 3"

            match opcode:
                case 1: # Add
                    assert mode[2] != 1
                    mem[raw_p[2]] = p[1] + p[0]
                    #print(f"Add {p[0]} to {p[1]} to @{p[2]}: {mem[raw_p[2]]}")
                    #print("",mem)
                    pc += 4

                case 2: # Multiply
                    assert mode[2] != 1
                    mem[raw_p[2]] = p[1] * p[0]
                    #print(f"Mul {p[0]} to {p[1]} to @{p[2]}: {mem[raw_p[2]]}")
                    #print("",mem)
                    pc += 4

                case 3: # Input
                    if len(self.inputs) == 0:
                        #if not self.state == SimState.BLOCKED_ON_INPUT:
                        #    print(f"{self.name} blocking on input")
                        self.state = SimState.BLOCKED_ON_INPUT
                        return
                    self.state = SimState.READY
                    assert mode[0] != 1
                    mem[raw_p[0]] = self.inputs.pop(0)
                    #print(f"Input {mem[raw_p[0]]}")
                    #print("",mem)
                    pc += 2

                case 4: # Output
                    #print(f"Output {p[0]}")
                    #print("",mem)
                    self.outputs.append(p[0])
                    pc += 2

                case 5: # Jump If True
                    if p[0]: pc = p[1]
                    else: pc += 3

                case 6: # Jump If False
                    if not p[0]: pc = p[1]
                    else: pc += 3

                case 7: # Less Than
                    val = 0
                    if p[0] < p[1]:
                        val = 1
                    mem[raw_p[2]] = val
                    pc += 4

                case 8: # Equals
                    val = 0
                    if p[0] == p[1]:
                        val = 1
                    mem[raw_p[2]] = val
                    pc += 4

                case 9: # Change Relative Base
                    self.rbase += p[0]
                    pc += 2

                case 99: # Halt
                    #print(f"Halting {self.name}")
                    #print("",mem)
                    self.state = SimState.HALTED
                    # pc += 1 # Hypothetically
                case _: #
                    assert False, f"Unknown opcode {mem[pc]} at PC {pc}"
            self.pc = pc

    def halted(self): return self.state == SimState.HALTED
    def ready(self): return self.state == SimState.READY
    def blocked_on_input(self): return self.state == SimState.BLOCKED_ON_INPUT

    def copy(self):
        new = IntcodeSimulator(self.memory, self.name, self.inputs[:])
        new.outputs = self.outputs[:]
        new.pc = self.pc
        new.state = self.state
        new.rbase = self.rbase
        return new

    def __repr__(self):
        return f"<Intcode pc={self.pc}, rbase={self.rbase}, inputs={self.inputs}, outputs={self.outputs}>"


def print_map(walls, clear, position, oxygen):
    minx = 0
    maxx = 0
    miny = 0
    maxy = 0
    for pos in walls | clear:
        minx = min(minx, pos.x)
        maxx = max(maxx, pos.x)
        miny = min(miny, pos.y)
        maxy = max(maxy, pos.y)

    for y in range(miny, maxy+1):
        line = ""
        for x in range(minx, maxx+1):
            p = Pt(x,y)
            if p in walls:
                line += "█"
            elif p == Pt(0,0):
                line += "O"
            elif oxygen is not None and p == oxygen:
                line += "X"
            elif p == position:
                line += "┼"
            elif p in clear:
                line += "·"
            else:
                line += " "
        print(line)
    print(position)
    print()



NORTH = Pt( 0, -1)
SOUTH = Pt( 0, +1)
EAST  = Pt(+1,  0)
WEST  = Pt(-1,  0)
DIRS = [NORTH, EAST, SOUTH, WEST]
DROID_DIRS = [1, 4, 2, 3]

files = advent.initialize(2023,15,1,"Oxygen System")#,["output"])

memory = list([int(x) for x in files.input.sreadline().split(",")]) + [0]*10000
sim = IntcodeSimulator(memory)

walls = set()
oxygen = None
clear = set([Pt(0,0)])
facing_idx = 0
todo = [(Pt(0,0),sim)]
while len(todo) > 0:
    position, this_sim = todo.pop(0)
    this_sim_memory_backup = this_sim.memory[:]
    #print(f"Consider {position}")
    for dir_idx in range(4):
        copy = this_sim.copy()
        copy.inputs.append(DROID_DIRS[dir_idx])
        potential_position = position + DIRS[dir_idx]
        if potential_position in walls or potential_position in clear:
            continue
        #print(f"   moving to {potential_position}")
        copy_memory_before = copy.memory[:]
        copy.run()
        copy_memory_after = copy.memory[:]
        out = copy.outputs.pop(0)
        if out == 0:
            #print(f"    blocked")
            walls.add(potential_position)
        else:
            assert out in [1,2]
            #print(f"    clear")
            clear.add(potential_position)
            if out == 2:
                #print(f"        and oxygen system")
                oxygen = potential_position
            todo.append( (potential_position, copy) )

assert oxygen is not None
print_map(walls, clear, position, oxygen)


sim.run()

